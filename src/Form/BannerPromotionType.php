<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Repository\CultureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BannerPromotionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'cms.field.title',
            ])
            ->add('description', null, [
                'label' => 'cms.field.description',
            ])
            ->add('button', null, [
                'label' => 'cms.field.button',
            ])
            ->add('image', FileType::class, [
                'label' => 'cms.field.image',
                'required' => false
            ])
            ->add('imageMobile', FileType::class, [
                'label' => 'cms.field.imageMobile',
                'required' => false
            ])
            ->add('url', null, [
                'label' => 'cms.field.url',
                'attr' => [
                    'placeholder' => 'cms.placeholder.banner_url'
                ]
            ])
            ->add('isActive', null, [
                'label' => 'cms.field.is_active',
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $bannerPromotion = $event->getData();
                $form = $event->getForm();

                if (!$bannerPromotion) {
                    return;
                }

                if (array_key_exists('image', $bannerPromotion)) {
                    if ($bannerPromotion['image'] == null && null !== $form->get('image')->getData()) {
                        $bannerPromotion['image'] = $form->get('image')->getData();
                        $event->setData($bannerPromotion);
                    }
                }

                if (array_key_exists('imageMobile', $bannerPromotion)) {
                    if ($bannerPromotion['imageMobile'] == null && null !== $form->get('imageMobile')->getData()) {
                        $bannerPromotion['imageMobile'] = $form->get('imageMobile')->getData();
                        $event->setData($bannerPromotion);
                    }
                }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\BannerPromotion',
            'translation_domain' => 'cms',
            'requireUpload' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bannerpromotion';
    }
}
