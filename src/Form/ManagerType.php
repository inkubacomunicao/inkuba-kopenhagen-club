<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ManagerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, ['label' => 'cms.field.email'])
        ;

        if ($options['password']) {
            $builder
                ->add('passwordText', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'cms.messages.invalid_password',
                    'required' => false,
                    'first_name' => 'password',
                    'second_name' => 'confirm',
                    'first_options'  => ['label' => 'cms.field.password', 'error_bubbling' => true],
                    'second_options' => ['label' => 'cms.field.confirm_password'], 'error_bubbling' => true,
                    'mapped' => true])
            ;
        }

        $builder
            ->add('isActive', null, ['label' => 'cms.field.is_active'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Manager',
            'password' => false,
            'translation_domain' => 'cms'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_manager';
    }
}
