<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PromotionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'cms.field.title',
            ])
            ->add('slug', null, [
                'label' => 'cms.field.slug',
            ])
            ->add('description', null, [
                'label' => 'cms.field.description',
            ])
            ->add('content', HiddenType::class, [
                'label' => 'cms.field.content',
            ])
            ->add('regulation', HiddenType::class, [
                'label' => 'cms.field.regulation',
            ])
            ->add('isActive', null, [
                'label' => 'cms.field.is_active',
            ])
            ->add('publishedAt', null, [
                'label' => 'cms.field.published_at',
            ])
            ->add('expiredAt', null, [
                'label' => 'cms.field.expired_at',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Promotion',
            'translation_domain' => 'cms',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_promotion';
    }
}
