<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Repository\CultureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BannerHomeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'label' => 'cms.field.title',
            ])
            ->add('description', null, [
                'label' => 'cms.field.description',
            ])
            ->add('button', null, [
                'label' => 'cms.field.button',
            ])
            ->add('image', FileType::class, [
                'label' => 'cms.field.image',
                'required' => false
            ])
            ->add('imageMobile', FileType::class, [
                'label' => 'cms.field.imageMobile',
                'required' => false
            ])
            ->add('url', null, [
                'label' => 'cms.field.url',
                'attr' => [
                    'placeholder' => 'cms.placeholder.banner_url'
                ]
            ])
            ->add('audience', ChoiceType::class, [
                'label' => 'cms.field.audience',
                'mapped' => true,
                'expanded' => true,
                'multiple' => true,
                'choices' => [
                    // 'cms.choice.none' => null,
                    // 'cms.choice.birthday' => 'birthday',
                    // 'cms.choice.expiring_points' => 'expiring_points',
                    'cms.choice.authenticated' => 'authenticated',
                    'cms.choice.anonymous' => 'anonymous'
                ]
            ])
            ->add('isActive', null, [
                'label' => 'cms.field.is_active',
            ])
            ->add('publishedAt', null, [
                'label' => 'cms.field.published_at',
            ])
            ->add('expiredAt', null, [
                'label' => 'cms.field.expired_at',
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $bannerHome = $event->getData();
                $form = $event->getForm();

                if (!$bannerHome) {
                    return;
                }

                if (array_key_exists('image', $bannerHome)) {
                    if ($bannerHome['image'] == null && null !== $form->get('image')->getData()) {
                        $bannerHome['image'] = $form->get('image')->getData();
                        $event->setData($bannerHome);
                    }
                }

                if (array_key_exists('imageMobile', $bannerHome)) {
                    if ($bannerHome['imageMobile'] == null && null !== $form->get('imageMobile')->getData()) {
                        $bannerHome['imageMobile'] = $form->get('imageMobile')->getData();
                        $event->setData($bannerHome);
                    }
                }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\BannerHome',
            'translation_domain' => 'cms',
            'requireUpload' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bannerhome';
    }
}
