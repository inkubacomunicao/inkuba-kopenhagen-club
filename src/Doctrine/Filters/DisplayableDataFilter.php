<?php

namespace App\Doctrine\Filters;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

class DisplayableDataFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $now = date('Y-m-d H:i:s');
        $statement = [];

        if (in_array('published_at', $targetEntity->getColumnNames())) {
            array_push($statement, "$targetTableAlias.published_at < '$now'");
        }

        if (in_array('expired_at', $targetEntity->getColumnNames())) {
            array_push($statement, "($targetTableAlias.expired_at IS NULL OR $targetTableAlias.expired_at > '$now')");
        }

        if (in_array('is_active', $targetEntity->getColumnNames())) {
            array_push($statement, "$targetTableAlias.is_active = 1");
        }

        return $statement ? join(' AND ', $statement) : "";
    }
}
