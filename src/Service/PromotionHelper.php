<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PromotionHelper
 *
 * @package App\Service
 */
class PromotionHelper
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getActive()
    {
        $this->em->getFilters()->enable('displayable_data');

        return $this->em->getRepository("App:Promotion")->findAll([], ['createdAt' => 'DESC']);
    }
}
