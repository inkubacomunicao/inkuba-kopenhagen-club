<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * ImageUploaderService
 */
class ImageUploaderService
{
    private $path;
    private $url;
    private $cacheManager;
    private $filterManager;
    private $filterService;

    /**
     * Constructor
     *
     * @param mixed $path
     * @param mixed $url
     * @param mixed $cacheManager
     * @param mixed $filterManager
     * @param mixed $filterService
     */
    public function __construct($path, $url, $cacheManager, $filterManager, $filterService)
    {
        $this->path = $path;
        $this->url = $url;
        $this->cacheManager = $cacheManager;
        $this->filterManager = $filterManager;
        $this->filterService = $filterService;
    }

    /**
     * Function save
     *
     * @param mixed $file
     *
     * @return void
     */
    public function save($file)
    {
        if (!$file) {
            return null;
        }

        if (!file_exists($this->path)) {
            mkdir($this->path, 0777, true);
        }

        if ($file->getExtension() !== 'tmp') {
            return $file->getFilename();
        }

        $extension = $file->guessExtension();

        $uniqueName = uniqid('kop_club_', true);

        $filename = $uniqueName.'.'.$extension;

        $this->saveAs($file, $filename);

        return $filename;
    }

    /**
     * Function saveAs
     *
     * @param mixed $file
     * @param mixed $name
     *
     * @return void
     */
    public function saveAs($file, $name)
    {
        if (!file_exists($this->path)) {
            mkdir($this->path, 0777, true);
        }

        if ($file) {
            $imagePath = $this->path.DIRECTORY_SEPARATOR.$name;
            $imageUrl = $this->url.'/'.$name;

            $image = new \Imagick();
            $image->readImage($file->getPathname());

            if (file_exists($imagePath)) {
                unlink($imagePath);
            }

            $image->writeImage($imagePath);

            $this->cacheManager->remove($imageUrl);

            foreach ($this->filterManager->getFilterConfiguration()->all() as $name => $config) {
                $this->filterService->getUrlOfFilteredImage($imageUrl, $name);
            }

            if (file_exists($file->getPathname())) {
                unlink($file->getPathname());
            }
        }
    }

    /**
     * Function delete
     *
     * @param mixed $name
     *
     * @return void
     */
    public function delete($name)
    {
        $imagePath = $this->path.DIRECTORY_SEPARATOR.$name;
        $imageUrl = $this->url.DIRECTORY_SEPARATOR.$name;

        if (file_exists($imagePath)) {
            unlink($imagePath);
            $this->cacheManager->remove($imageUrl);
        }
    }

    /**
     * Function retrieve
     *
     * @param mixed $filename
     *
     * @return void
     */
    public function retrieve($filename)
    {
        $fullPath = $this->path.DIRECTORY_SEPARATOR.$filename;

        return file_exists($fullPath) && null !== $filename ? new File($fullPath) : null;
    }
}
