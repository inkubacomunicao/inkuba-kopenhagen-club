<?php

namespace App\Controller\Cms;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use App\Entity\Manager;
use App\Datatables\ManagerDatatable;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;


/**
 * Manager controller.
 *
 * @Route("cms/manager")
 */
class ManagerController extends AbstractController
{
    private $dtFactory;
    private $dtResponse;

    /**
     * Constructor
     *
     * @param ImageUploaderService $imageUploaderService
     */
    public function __construct(DatatableFactory $datatableFactory, DatatableResponse $datatableResponse)
    {
        $this->dtFactory = $datatableFactory;
        $this->dtResponse = $datatableResponse;
    }

    /**
     * Lists all manager entities.
     *
     * @Route("/", name="cms_manager_index", methods={"GET","POST"})
     */
    public function indexAction(Request $request)
    {
        $datatable = $this->dtFactory->create(ManagerDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return $this->render('cms/manager/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * Creates a new manager entity.
     *
     * @Route("/new", name="cms_manager_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, TranslatorInterface $translator)
    {
        $manager = new Manager();
        $form = $this->createForm('App\Form\ManagerType', $manager, ['password' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $em = $this->getDoctrine()->getManager();
                $manager->setPassword($this->encodePassword($manager));
                $em->persist($manager);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;

                return $this->redirectToRoute('cms_manager_show', array('id' => $manager->getId()));
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms') . $ex->getMessage())
                ;
            }
        }

        return $this->render('cms/manager/new.html.twig', array(
            'manager' => $manager,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a manager entity.
     *
     * @Route("/{id}", name="cms_manager_show", methods={"GET","POST"})
     */
    public function showAction(Request $request, Manager $manager)
    {
        $deleteForm = $this->createDeleteForm($manager);

        return $this->render('cms/manager/show.html.twig', array(
            'manager' => $manager,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing manager entity.
     *
     * @Route("/{id}/edit", name="cms_manager_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Manager $manager, TranslatorInterface $translator)
    {
        $deleteForm = $this->createDeleteForm($manager);
        $editForm = $this->createForm('App\Form\ManagerType', $manager, ['password' => true]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();

            try {
                if ($manager->getPasswordText()) {
                    $manager->setPassword($this->encodePassword($manager));
                }

                $this->getDoctrine()->getManager()->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms') . $ex->getMessage())
                ;
            }

            return $this->redirectToRoute('cms_manager_edit', array('id' => $manager->getId()));
        }

        return $this->render('cms/manager/edit.html.twig', array(
            'manager' => $manager,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a manager entity.
     *
     * @Route("/{id}", name="cms_manager_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Manager $manager, TranslatorInterface $translator)
    {
        $form = $this->createDeleteForm($manager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($manager);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms') . $ex->getMessage())
                ;
            }
        }

        return $this->redirectToRoute('cms_manager_index');
    }

    /**
     * Creates a form to delete a manager entity.
     *
     * @param Manager $manager The manager entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Manager $manager)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_manager_delete', array('id' => $manager->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Encodes the password.
     *
     * @param string Plain Text Password
     *
     * @return string Encoded Password
     */
    private function encodePassword($manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        return $encoder->encodePassword($manager, $manager->getPasswordText());
    }
}
