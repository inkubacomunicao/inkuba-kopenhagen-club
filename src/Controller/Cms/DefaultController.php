<?php

namespace App\Controller\Cms;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Default controller.
 *
 * @Route("cms")
 */
class DefaultController extends AbstractController
{
    /**
     * Manager welcome page.
     *
     * @Route("/", name="cms_default_index", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->redirectToRoute('cms_bannerhome_index');
    }
}
