<?php

namespace App\Controller\Cms;

use App\Entity\Promotion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use App\Datatables\PromotionDatatable;
use App\Datatables\BannerPromotionDatatable;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;

/**
 * Promotion controller.
 *
 * @Route("cms/promotion")
 */
class PromotionController extends AbstractController
{
    private $imageUploadDirectory;
    private $imageUploadFolder;
    private $dtFactory;
    private $dtResponse;

    /**
     * Constructor
     *
     * @param String $imageUploadDirectory
     * @param String $imageUploadFolder
     */
    public function __construct(DatatableFactory $datatableFactory, DatatableResponse $datatableResponse, String $imageUploadDirectory, String $imageUploadFolder)
    {
        $this->imageUploadDirectory = $imageUploadDirectory;
        $this->imageUploadFolder = $imageUploadFolder;
        $this->dtFactory = $datatableFactory;
        $this->dtResponse = $datatableResponse;
    }

    /**
     * Lists all promotion entities.
     *
     * @Route("/", name="cms_promotion_index", methods={"GET","POST"})
     */
    public function indexAction(Request $request)
    {
        $datatable = $this->dtFactory->create(PromotionDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return $this->render('cms/promotion/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * Creates a new promotion entity.
     *
     * @Route("/new", name="cms_promotion_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, TranslatorInterface $translator)
    {
        $promotion = new Promotion();
        $form = $this->createForm('App\Form\PromotionType', $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($promotion);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;

                return $this->redirectToRoute('cms_promotion_edit', array('id' => $promotion->getId()));
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms') . $ex->getMessage())
                ;
            }
        }

        return $this->render('cms/promotion/new.html.twig', array(
            'promotion' => $promotion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a promotion entity.
     *
     * @Route("/{id}", name="cms_promotion_show", methods={"GET"})
     */
    public function showAction(Promotion $promotion)
    {
        $deleteForm = $this->createDeleteForm($promotion);

        return $this->render('cms/promotion/show.html.twig', array(
            'promotion' => $promotion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing promotion entity.
     *
     * @Route("/{id}/edit", name="cms_promotion_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Promotion $promotion, TranslatorInterface $translator)
    {
        $deleteForm = $this->createDeleteForm($promotion);
        $editForm = $this->createForm('App\Form\PromotionType', $promotion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();

            try {
                $this->getDoctrine()->getManager()->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms'))
                ;
            }

            return $this->redirectToRoute('cms_promotion_edit', array('id' => $promotion->getId()));
        }

        $imagePath = $this->imageUploadFolder;
        $datatable = $this->dtFactory->create(BannerPromotionDatatable::class);
        $datatable->buildDatatable(['imagePath' => $imagePath, 'promotionId' => $promotion->getId()]);

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);

            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();
            $qb = $datatableQueryBuilder->getQb();
            $qb
                ->join('bannerpromotion.promotion', 'p')
                ->andWhere('p.id = :promotionId')
                ->setParameter('promotionId', $promotion->getId())
            ;

            return $responseService->getResponse();
        }

        return $this->render('cms/promotion/edit.html.twig', array(
            'promotion' => $promotion,
            'datatable' => $datatable,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a promotion entity.
     *
     * @Route("/{id}", name="cms_promotion_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Promotion $promotion)
    {
        $form = $this->createDeleteForm($promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($promotion);
            $em->flush();
        }

        return $this->redirectToRoute('cms_promotion_index');
    }

    /**
     * Creates a form to delete a promotion entity.
     *
     * @param Promotion $promotion The promotion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Promotion $promotion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_promotion_delete', array('id' => $promotion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
