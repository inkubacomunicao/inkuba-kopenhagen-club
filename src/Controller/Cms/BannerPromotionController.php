<?php

namespace App\Controller\Cms;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\File\File;
use App\Service\ImageUploaderService;
use App\Entity\BannerPromotion;
use App\Entity\Promotion;
use App\Datatables\BannerPromotionDatatable;

/**
 * BannerPromotion controller.
 *
 * @Route("cms/promotion")
 */
class BannerPromotionController extends AbstractController
{
    private $imageUploadDirectory;
    private $imageUploadFolder;
    private $imageUploaderService;

    /**
     * Constructor
     *
     * @param ImageUploaderService $imageUploaderService
     * @param String $imageUploadDirectory
     * @param String $imageUploadFolder
     */
    public function __construct(ImageUploaderService $imageUploaderService, String $imageUploadDirectory, String $imageUploadFolder)
    {
        $this->imageUploaderService = $imageUploaderService;
        $this->imageUploadDirectory = $imageUploadDirectory;
        $this->imageUploadFolder = $imageUploadFolder;
    }

    /**
     * Creates a new BannerPromotion entity.
     *
     * @Route("/{promotionId}/banner/new", name="cms_bannerpromotion_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, TranslatorInterface $translator, $promotionId)
    {
        $em = $this->getDoctrine()->getManager();

        $bannerPromotion = new BannerPromotion();
        $bannerPromotion->setPromotion($em->getReference(Promotion::class, $promotionId));

        $form = $this->createForm('App\Form\BannerPromotionType', $bannerPromotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $repository = $this->getDoctrine()->getManager()->getRepository('App:BannerPromotion');

                $uploader = $this->imageUploaderService;

                $bannerPromotion->setImage($uploader->save($bannerPromotion->getImage()));
                $bannerPromotion->setImageMobile($uploader->save($bannerPromotion->getImageMobile()));
                $bannerPromotion->setOrdenator($repository->findMaxOrdenator($promotionId) + 1);

                $em->persist($bannerPromotion);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;

                return $this->redirectToRoute('cms_promotion_show', ['id' => $bannerPromotion->getPromotion()->getId()]);
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms').$ex->getMessage())
                ;
            }
        }

        return $this->render('cms/promotion-banner/new.html.twig', array(
            'bannerPromotion' => $bannerPromotion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BannerPromotion entity.
     *
     * @Route("/banner/{id}", name="cms_bannerpromotion_show", methods={"GET"})
     */
    public function showAction(BannerPromotion $bannerPromotion)
    {
        if ($bannerPromotion->getImage()) {
            $image = $this->imageUploadDirectory.DIRECTORY_SEPARATOR.$bannerPromotion->getImage();
            $bannerPromotion->setImage(new File($image));
        }

        if ($bannerPromotion->getImageMobile()) {
            $imageMobile = $this->imageUploadDirectory.DIRECTORY_SEPARATOR.$bannerPromotion->getImageMobile();
            $bannerPromotion->setImageMobile(new File($imageMobile));
        }

        $deleteForm = $this->createDeleteForm($bannerPromotion);

        return $this->render('cms/promotion-banner/show.html.twig', array(
            'bannerPromotion' => $bannerPromotion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BannerPromotion entity.
     *
     * @Route("/banner/{id}/edit", name="cms_bannerpromotion_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, BannerPromotion $bannerPromotion, TranslatorInterface $translator)
    {
        $uploader = $this->imageUploaderService;

        $bannerPromotion->setImage($uploader->retrieve($bannerPromotion->getImage()));
        $bannerPromotion->setImageMobile($uploader->retrieve($bannerPromotion->getImageMobile()));

        $deleteForm = $this->createDeleteForm($bannerPromotion);
        $editForm = $this->createForm('App\Form\BannerPromotionType', $bannerPromotion, [ 'requireUpload' => false ]);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();

            try {
                if ($bannerPromotion->getImage() != null && $image = $uploader->save($bannerPromotion->getImage())) {
                    $bannerPromotion->setImage($image);
                }

                if ($bannerPromotion->getImageMobile() != null && $imageMobile = $uploader->save($bannerPromotion->getImageMobile())) {
                    $bannerPromotion->setImageMobile($imageMobile);
                }

                $this->getDoctrine()->getManager()->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;

                return $this->redirectToRoute('cms_promotion_show', ['id' => $bannerPromotion->getPromotion()->getId()]);
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms').$ex->getMessage())
                ;
            }

            return $this->redirectToRoute('cms_bannerpromotion_edit', array('id' => $bannerPromotion->getId()));
        }

        return $this->render('cms/promotion-banner/edit.html.twig', array(
            'bannerPromotion' => $bannerPromotion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a BannerPromotion entity.
     *
     * @Route("/banner/{id}", name="cms_bannerpromotion_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, BannerPromotion $bannerPromotion, TranslatorInterface $translator)
    {
        $form = $this->createDeleteForm($bannerPromotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($bannerPromotion);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_removing', [], 'cms'))
                ;
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_removing', [], 'cms').$ex->getMessage())
                ;
            }
        }

        return $this->redirectToRoute('cms_bannerpromotion_index');
    }

    /**
     * Creates a form to delete a BannerPromotion entity.
     *
     * @param BannerPromotion $bannerPromotion The BannerPromotion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BannerPromotion $bannerPromotion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_bannerpromotion_delete', array('id' => $bannerPromotion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
