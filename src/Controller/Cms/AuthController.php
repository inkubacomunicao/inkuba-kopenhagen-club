<?php

namespace App\Controller\Cms;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Authentication controller.
 *
 * @Route("cms/auth")
 */
class AuthController extends AbstractController
{
    /**
     * @Route("/login", name="cms_auth_login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render('cms/auth/login.html.twig', [
            'last_email' => $lastEmail,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="cms_auth_logout")
     */
    public function logoutAction(Request $request)
    {
        return $this->render('cms/auth/logout.html.twig', []);
    }
}
