<?php

namespace App\Controller\Cms;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\File\File;
use App\Service\ImageUploaderService;
use App\Entity\BannerHome;
use App\Datatables\BannerHomeDatatable;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;

/**
 * BannerHome controller.
 *
 * @Route("cms/banner")
 */
class BannerHomeController extends AbstractController
{
    private $imageUploaderService;
    private $imageUploadDirectory;
    private $imageUploadFolder;
    private $dtFactory;
    private $dtResponse;

    /**
     * Constructor
     *
     * @param ImageUploaderService $imageUploaderService
     * @param DatatableFactory $datatableFactory
     * @param DatatableResponse $datatableResponse
     * @param String $imageUploadDirectory
     * @param String $imageUploadFolder
     */
    public function __construct(ImageUploaderService $imageUploaderService, DatatableFactory $datatableFactory, DatatableResponse $datatableResponse, String $imageUploadDirectory, String $imageUploadFolder)
    {
        $this->imageUploaderService = $imageUploaderService;
        $this->imageUploadDirectory = $imageUploadDirectory;
        $this->imageUploadFolder = $imageUploadFolder;
        $this->dtFactory = $datatableFactory;
        $this->dtResponse = $datatableResponse;
    }

    /**
     * Lists all BannerHome entities.
     *
     * @Route("/", name="cms_bannerhome_index", methods={"GET","POST"})
     */
    public function indexAction(Request $request)
    {
        $imagePath = $this->imageUploadFolder;
        $datatable = $this->dtFactory->create(BannerHomeDatatable::class);
        $datatable->buildDatatable(['imagePath' => $imagePath]);

        if ($request->isXmlHttpRequest()) {
            $responseService = $this->dtResponse;
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return $this->render('cms/banner/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * Creates a new BannerHome entity.
     *
     * @Route("/new", name="cms_bannerhome_new", methods={"GET","POST"})
     */
    public function newAction(Request $request, TranslatorInterface $translator)
    {
        $bannerHome = new BannerHome();
        $form = $this->createForm('App\Form\BannerHomeType', $bannerHome);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $this->getDoctrine()->getManager()->getRepository('App:BannerHome');

                $uploader = $this->imageUploaderService;

                $bannerHome->setImage($uploader->save($bannerHome->getImage()));
                $bannerHome->setImageMobile($uploader->save($bannerHome->getImageMobile()));
                $bannerHome->setOrdenator($repository->findMaxOrdenator() + 1);

                $em->persist($bannerHome);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;

                return $this->redirectToRoute('cms_bannerhome_index');
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms').$ex->getMessage())
                ;
            }
        }

        return $this->render('cms/banner/new.html.twig', array(
            'bannerHome' => $bannerHome,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BannerHome entity.
     *
     * @Route("/{id}", name="cms_bannerhome_show", methods={"GET"})
     */
    public function showAction(BannerHome $bannerHome)
    {
        if ($bannerHome->getImage()) {
            $image = $this->imageUploadDirectory.DIRECTORY_SEPARATOR.$bannerHome->getImage();
            $bannerHome->setImage(new File($image));
        }

        if ($bannerHome->getImageMobile()) {
            $imageMobile = $this->imageUploadDirectory.DIRECTORY_SEPARATOR.$bannerHome->getImageMobile();
            $bannerHome->setImageMobile(new File($imageMobile));
        }

        $deleteForm = $this->createDeleteForm($bannerHome);

        return $this->render('cms/banner/show.html.twig', array(
            'bannerHome' => $bannerHome,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BannerHome entity.
     *
     * @Route("/{id}/edit", name="cms_bannerhome_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, BannerHome $bannerHome, TranslatorInterface $translator)
    {
        $uploader = $this->imageUploaderService;

        $bannerHome->setImage($uploader->retrieve($bannerHome->getImage()));
        $bannerHome->setImageMobile($uploader->retrieve($bannerHome->getImageMobile()));

        $deleteForm = $this->createDeleteForm($bannerHome);
        $editForm = $this->createForm('App\Form\BannerHomeType', $bannerHome, [ 'requireUpload' => false ]);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $session = new Session();

            try {
                if ($bannerHome->getImage() != null && $image = $uploader->save($bannerHome->getImage())) {
                    $bannerHome->setImage($image);
                }

                if ($bannerHome->getImageMobile() != null && $imageMobile = $uploader->save($bannerHome->getImageMobile())) {
                    $bannerHome->setImageMobile($imageMobile);
                }

                $this->getDoctrine()->getManager()->flush();

                $bannerHome->setImage($uploader->retrieve($bannerHome->getImage()));
                $bannerHome->setImageMobile($uploader->retrieve($bannerHome->getImageMobile()));

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_saving', [], 'cms'))
                ;

                return $this->redirectToRoute('cms_bannerhome_index');
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_saving', [], 'cms').$ex->getMessage())
                ;
            }

            return $this->redirectToRoute('cms_bannerhome_edit', array('id' => $bannerHome->getId()));
        }

        return $this->render('cms/banner/edit.html.twig', array(
            'bannerHome' => $bannerHome,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a BannerHome entity.
     *
     * @Route("/{id}", name="cms_bannerhome_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, BannerHome $bannerHome, TranslatorInterface $translator)
    {
        $form = $this->createDeleteForm($bannerHome);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();

            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($bannerHome);
                $em->flush();

                $session
                    ->getFlashBag()
                    ->add('success', $translator->trans('cms.message.success_removing', [], 'cms'))
                ;
            } catch (\Exception $ex) {
                $session
                    ->getFlashBag()
                    ->add('error', $translator->trans('cms.message.error_removing', [], 'cms').$ex->getMessage())
                ;
            }
        }

        return $this->redirectToRoute('cms_bannerhome_index');
    }

    /**
     * Creates a form to delete a BannerHome entity.
     *
     * @param BannerHome $bannerHome The BannerHome entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BannerHome $bannerHome)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_bannerhome_delete', array('id' => $bannerHome->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
