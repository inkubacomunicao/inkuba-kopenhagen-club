<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Delete;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validation;

/**
 * Participant controller.
 *
 * @Route("api/participant")
 */
class ParticipantController extends AbstractFOSRestController
{
    private $client;
    private $apiBaseUrl;
    private $apiUsername;
    private $apiPassword;
    private $secret;

    /**
     * Constructor
     *
     * @param HttpClientInterface $client
     * @param string              $apiBaseUrl
     * @param string              $apiUsername
     * @param string              $apiPassword
     * @param string              $secret
     */
    public function __construct(HttpClientInterface $client, String $apiBaseUrl, string $apiUsername, string $apiPassword, string $secret)
    {
        $this->client = $client;
        $this->apiBaseUrl = $apiBaseUrl;
        $this->apiUsername = $apiUsername;
        $this->apiPassword = $apiPassword;
        $this->secret = $secret;
    }

    /**
     * @Get("/statement",
     *      name="api_participant_statement",
     *      defaults={ "_format" = "json" })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function statement(Request $request) : Response
    {
        try {
            $authorization = preg_replace('/Bearer\s/i', '', $request->headers->get('Authorization'));
            $jwt = (array) JWT::decode($authorization, $this->secret, ['HS256']);

            $view = $this->view($this->getStatement($jwt['cpf']), 200);
        } catch (\Exception $ex) {
            $view = $this->view(['message' => $ex->getMessage()], $ex->getCode() < 100 ? 500 : $ex->getCode());
        }

        return $this->handleView($view);
    }

    /**
     * @Get("/vouchers",
     *      name="api_participant_vouchers",
     *      defaults={ "_format" = "json" })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function vouchers(Request $request) : Response
    {
        try {
            $authorization = preg_replace('/Bearer\s/i', '', $request->headers->get('Authorization'));
            $jwt = (array) JWT::decode($authorization, $this->secret, ['HS256']);

            $view = $this->view($this->getVouchers($jwt['cpf']), 200);
        } catch (\Exception $ex) {
            $view = $this->view(['message' => $ex->getMessage()], $ex->getCode() < 100 ? 500 : $ex->getCode());
        }

        return $this->handleView($view);
    }

    /**
     * Function getStatement
     *
     * @param string $cpf
     *
     * @return Array
     */
    private function getStatement(string $cpf) : Array
    {
        return $this->fetch([
            'headers' => [ 'Content-Type' => 'application/json' ],
            'method' => 'GET',
            'url' => '/Site/GetStatement?cpf='.$cpf,
            'token' => $this->getAuthorizationToken(),
        ]);
    }

    /**
     * Function getVouchers
     *
     * @param string $cpf
     *
     * @return Array
     */
    private function getVouchers(string $cpf) : Array
    {
        return $this->fetch([
            'headers' => [ 'Content-Type' => 'application/json' ],
            'method' => 'GET',
            'url' => '/Site/GetVouchers?cpf='.$cpf,
            'token' => $this->getAuthorizationToken(),
        ]);
    }

    /**
     * Function getAuthorizationToken
     *
     * @return string
     */
    private function getAuthorizationToken() : string
    {
        $response = $this->fetch([
            'headers' => [ 'Content-Type' => 'application/json' ],
            'method' => 'POST',
            'json' => [
                'username' => $this->apiUsername,
                'password' => $this->apiPassword,
            ],
            'url' => '/User',
        ]);

        preg_match_all('/"token: (.*?)"/i', $response, $matches);

        return $matches[1][0];
    }

    /**
     * Function fetch
     *
     * @param Array $params
     *
     * @return Array|String
     */
    private function fetch(Array $params)
    {
        if (!$params || !is_array($params)) {
            throw new \Exception('Request params missing.');
        }

        if (!$this->apiBaseUrl) {
            throw new \Exception('Base API URL not set-up.');
        }

        if (!array_key_exists('url', $params)) {
            throw new \Exception('URL is missing');
        }

        if (!array_key_exists('method', $params)) {
            $params['method'] = 'GET';
        } else {
            $params['method'] = strtoupper($params['method']);
        }

        if (!array_key_exists('headers', $params)) {
            $data['headers'] = [];
        } else {
            $data['headers'] = $params['headers'];
        }

        if (array_key_exists('token', $params)) {
            array_push($data['headers'], 'Authorization: Bearer '.$params['token']);
        }

        if (array_key_exists('json', $params)) {
            $data['json'] = $params['json'];
        }

        try {
            $response = $this->client->request($params['method'], $this->apiBaseUrl.$params['url'], $data);

            $statusCode = $response->getStatusCode();

            try {
                $result = $response->toArray(false);
            } catch (\Exception $ex) {
                $result = $response->getContent(false);
            }

            if ($statusCode < 200 || $statusCode > 299) {
                throw new \Exception(JWT::jsonEncode($result), $statusCode);
            }

            return $result;
        } catch (\Exception $ex) {
            throw new \Exception('Sistema de pontos indisponível. Tente novamente mais tarde. [CAD001]', 424);
        }
    }
}
