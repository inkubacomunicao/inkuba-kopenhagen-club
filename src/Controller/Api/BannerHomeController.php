<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Context\Context;
use App\Entity\BannerHome;
use Firebase\JWT\JWT;

/**
 * BannerHome controller.
 *
 * @Route("api/banner")
 */
class BannerHomeController extends AbstractFOSRestController
{
    /**
     * @Post("/{id}/ordenator/{direction}",
     *      name="api_bannerhome_ordenator",
     *      defaults={ "_format" = "json" },
     *      requirements={ "id": "\d+", "direction": "up|down" })
     */
    public function postBannerHomeOrdenator(BannerHome $bannerHome, $direction)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $repo = $this->getDoctrine()->getManager()->getRepository('App:BannerHome');

            $previousOrdenator = $bannerHome->getOrdenator();
            $newOrdenator = $bannerHome->getOrdenator();

            switch ($direction) {
                case 'up':
                    $newOrdenator = $repo->findNewOrdenator($bannerHome->getId(), 1);
                    break;
                case 'down':
                    $newOrdenator = $repo->findNewOrdenator($bannerHome->getId(), -1);
                    break;
                default:
                    throw new Exception('Invalid argument for direction.', 1);
                    break;
            }

            $target = $repo->findOneByOrdenator($newOrdenator);

            $target->setOrdenator($previousOrdenator);
            $bannerHome->setOrdenator($newOrdenator);

            $em->persist($target);
            $em->persist($bannerHome);

            $em->flush();

            $view = $this->view($bannerHome, 201);
        } catch (\Exception $ex) {
            $view = $this->view([
                'code' => 500,
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage()
            ], 500);
        }

        return $this->handleView($view);
    }

    /**
     * @Post("/{id}/isactive/{status}",
     *      name="api_bannerhome_active",
     *      defaults={ "_format" = "json" },
     *      requirements={ "id": "\d+", "status": "1|0" })
     */
    public function postBannerHomeActive(BannerHome $bannerHome, $status)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $context = new Context();

            $bannerHome->setIsActive($status);

            $em->persist($bannerHome);
            $em->flush();

            $view = $this->view($bannerHome, 201);
        } catch (\Exception $ex) {
            $view = $this->view([
                'code' => 500,
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage()
            ], 500);
        }

        return $this->handleView($view);
    }

    /**
     * @Get("/contextual",
     *      name="api_bannerhome_list",
     *      defaults={ "_format" = "json" })
     */
    public function getBannerHomeAction(Request $request)
    {
        try {
            $token = preg_replace('/Bearer\s/i', '', $request->headers->get('Authorization'));
            $audience = [];

            if ($token) {
                list($headb64, $bodyb64, $cryptob64) = explode('.', $token);
                $payload = (array) JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64));

                if ($payload) {
                    array_push($audience, 'authenticated');
                } else {
                    array_push($audience, 'anonymous');
                }
            } else {
                array_push($audience, 'anonymous');
            }

            $em = $this->getDoctrine()->getManager();
            $em->getFilters()->enable('displayable_data');

            $banners = $em->getRepository('App:BannerHome')->findBy([], ['ordenator' => 'DESC']);

            $banners = array_values(array_filter($banners, function ($item) use ($audience) {
                if ($item->getAudience()) {
                    foreach ($item->getAudience() as $value) {
                        if (in_array($value, $audience)) {
                            return true;
                        }
                    }
                }

                return false;
            }));

            $view = $this->view($banners, 200);
        } catch (\Exception $ex) {
            $view = $this->view([
                'code' => 500,
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage(),
            ], 500);
        }

        return $this->handleView($view);
    }
}
