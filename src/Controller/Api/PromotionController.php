<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Context\Context;
use App\Entity\Promotion;

/**
 * Promotion controller.
 *
 * @Route("api/promotion")
 */
class PromotionController extends AbstractFOSRestController
{
    /**
     * @Post("/{id}/isactive/{status}",
     *      name="api_promotion_active",
     *      defaults={ "_format" = "json" },
     *      requirements={ "id": "^[A-Za-z0-9\-]+$", "status": "1|0" })
     */
    public function postPromotionActive(Promotion $promotion, $status)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $context = new Context();

            $promotion->setIsActive($status);

            $em->persist($promotion);
            $em->flush();

            $view = $this->view($promotion, 201);
        } catch (\Exception $ex) {
            $view = $this->view([
                'code' => 500,
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage()
            ], 500);
        }

        return $this->handleView($view);
    }
}
