<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Delete;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validation;

/**
 * Participant controller.
 *
 * @Route("api/v2/participant")
 */
class ParticipantV2Controller extends AbstractFOSRestController
{
    private $client;
    private $authBaseUrl;
    private $authClientID;
    private $authClientSecret;
    private $secret;

    /**
     * Constructor
     *
     * @param HttpClientInterface $client
     * @param string              $authBaseUrl
     * @param string              $authClientID
     * @param string              $authClientSecret
     * @param string              $secret
     */
    public function __construct(HttpClientInterface $client, string $authBaseUrl, string $authClientID, string $authClientSecret, string $secret)
    {
        $this->client = $client;
        $this->authBaseUrl = $authBaseUrl;
        $this->authClientID = $authClientID;
        $this->authClientSecret = $authClientSecret;
        $this->secret = $secret;
    }

    /**
     * @Post("/login", name="api_participant_login", defaults={ "_format" = "json" })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request) : Response
    {
        try {
            $content = (array) JWT::jsonDecode($request->getContent());
            $authorization = $this->getAccessToken($content['code']);
            $accessToken = $authorization['accessToken'];

            $user = $this->getUserInfo($accessToken);

            $payload = [
                "name" => $user['name'],
                "brithdate" => $user['birthDate'],
                "cpf" => $user['cpf'],
                "accessToken" => $accessToken,
                "CompletedRegistry" => true, // Não vem essa informação na API nova, portando forçamos o valor true.
                "iat" => time(),
                "nbf" => time(),
                "exp" => time() + $authorization['expiresIn'],
            ];

            $view = $this->view(['jwt' => JWT::encode($payload, $this->secret, 'HS256')], 200);
        } catch (\Exception $ex) {
            $view = $this->view(['message' => $ex->getMessage()], $ex->getCode() < 100 ? 500 : $ex->getCode());
        }

        return $this->handleView($view);
    }

    /**
     * @Get("/me", name="api_participant_me", defaults={ "_format" = "json" })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function me(Request $request) : Response
    {
        try {
            $authorization = preg_replace('/Bearer\s/i', '', $request->headers->get('Authorization'));
            $jwt = (array) JWT::decode($authorization, $this->secret, ['HS256']);

            $user = $this->getUserInfo($jwt['accessToken']);

            $view = $this->view([
                'receive_Email' => $user['optins']['email'],
                'receive_SMS' => $user['optins']['sms'],
                'receive_AppMSG' => $user['optins']['whatsapp'],
            ], 200);
        } catch (\Exception $ex) {
            $view = $this->view(['message' => $ex->getMessage()], $ex->getCode() < 100 ? 500 : $ex->getCode());
        }

        return $this->handleView($view);
    }


    /**
     * @Patch("/me", name="api_participant_optin", defaults={ "_format" = "json" })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function optin(Request $request) : Response
    {
        try {
            $content = (array) JWT::jsonDecode($request->getContent());
            $authorization = preg_replace('/Bearer\s/i', '', $request->headers->get('Authorization'));
            $jwt = (array) JWT::decode($authorization, $this->secret, ['HS256']);

            $this->setOptin($jwt['accessToken'], $content);

            $view = $this->view(null, 204);
        } catch (\Exception $ex) {
            $view = $this->view(['message' => $ex->getMessage()], $ex->getCode() < 100 ? 500 : $ex->getCode());
        }

        return $this->handleView($view);
    }

    /**
     * @Get("/update", name="api_participant_update", defaults={ "_format" = "json" })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request) : Response
    {
        try {
            $authorization = preg_replace('/Bearer\s/i', '', $request->headers->get('Authorization'));
            $jwt = (array) JWT::decode($authorization, $this->secret, ['HS256']);

            $response = $this->getUpdateURL($jwt['accessToken']);
            $response['url'] = $response['url'] ? $response['url'].'&redirect_uri='.urlencode($this->generateUrl('site_callback', [ 'state' => 'update' ], UrlGeneratorInterface::ABSOLUTE_URL)) : null;

            $view = $this->view($response, 200);
        } catch (\Exception $ex) {
            $view = $this->view(['message' => $ex->getMessage()], $ex->getCode() < 100 ? 500 : $ex->getCode());
        }

        return $this->handleView($view);
    }

    /**
     * Function getAccessToken
     *
     * @param string $accessCode
     *
     * @return Array
     */
    private function getAccessToken(string $accessCode) : Array
    {
        $payload = [
            'grantType' => 'authorization_code',
            'redirectUri' => $this->generateUrl('site_callback', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'code' => $accessCode,
        ];

        return $this->fetch([
            'method' => 'POST',
            'json' => $payload,
            'url' => '/api/auth/token?clientId='.$this->authClientID.'&clientSecret='.$this->authClientSecret,
        ]);
    }

    /**
     * Function getUpdateURL
     *
     * @param string $accessCode
     *
     * @return Array
     */
    private function getUpdateURL(string $accessCode) : Array
    {
        return $this->fetch([
            'method' => 'GET',
            'url' => '/api/user/signed-url?clientId='.$this->authClientID.'&clientSecret='.$this->authClientSecret,
            'token' => $accessCode,
        ]);
    }

    /**
     * Function getUserInfo
     *
     * @param string $accessToken
     *
     * @return Array
     */
    private function getUserInfo(string $accessToken) : Array
    {
        return $this->fetch([
            'method' => 'GET',
            'token' => $accessToken,
            'url' => '/api/user',
        ]);
    }

    /**
     * Function setOptin
     *
     * @param string $accessCode
     * @param array  $data
     *
     * @return Array
     */
    private function setOptin(string $accessCode, array $data) : Array
    {
        $payload['optins'] = [
            'whatsapp' => $data['receive_AppMSG'],
            'email' =>  $data['receive_Email'],
            'sms' =>  $data['receive_Sms'],
        ];

        return $this->fetch([
            'method' => 'PATCH',
            'json' => $payload,
            'url' => '/api/user',
            'token' => $accessCode,
        ]);
    }

    /**
     * Function fetch
     *
     * @param Array $params
     *
     * @return Array
     */
    private function fetch(Array $params) : Array
    {
        if (!$params || !is_array($params)) {
            throw new \Exception('Request params missing.');
        }

        if (!$this->authBaseUrl) {
            throw new \Exception('Base API URL not set-up.');
        }

        if (!array_key_exists('url', $params)) {
            throw new \Exception('URL is missing');
        }

        if (!array_key_exists('method', $params)) {
            $params['method'] = 'GET';
        } else {
            $params['method'] = strtoupper($params['method']);
        }

        if (!array_key_exists('headers', $params)) {
            $data['headers'] = [];
        } else {
            $data['headers'] = $params['headers'];
        }

        if (array_key_exists('token', $params)) {
            array_push($data['headers'], 'Authorization: Bearer '.$params['token']);
        }

        if (array_key_exists('json', $params)) {
            $data['json'] = $params['json'];
        }

        try {
            $response = $this->client->request($params['method'], $this->authBaseUrl.$params['url'], $data);

            $statusCode = $response->getStatusCode();
            $result = $response->toArray(false);

            if ($statusCode < 200 || $statusCode > 299) {
                throw new \Exception(JWT::jsonEncode($result), $statusCode);
            }

            return $result;
        } catch (\Exception $ex) {
            throw new \Exception('Sistema de autenticação indisponível. Tente novamente mais tarde. [TAQ001]', 424);
        }
    }
}
