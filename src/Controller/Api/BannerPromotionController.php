<?php

namespace App\Controller\Api;

use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Context\Context;
use App\Entity\BannerPromotion;

/**
 * BannerPromotion controller.
 *
 * @Route("api/promotion/banner")
 */
class BannerPromotionController extends AbstractFOSRestController
{
    /**
     * @Post("/{id}/ordenator/{direction}",
     *      name="api_bannerpromotion_ordenator",
     *      defaults={ "_format" = "json" },
     *      requirements={ "id": "\d+", "direction": "up|down" })
     */
    public function postBannerPromotionOrdenator(BannerPromotion $bannerPromotion, $direction)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $repo = $this->getDoctrine()->getManager()->getRepository('App:BannerPromotion');

            $previousOrdenator = $bannerPromotion->getOrdenator();
            $newOrdenator = $bannerPromotion->getOrdenator();

            switch ($direction) {
                case 'up':
                    $newOrdenator = $repo->findNewOrdenator($bannerPromotion->getId(), 1);
                    break;
                case 'down':
                    $newOrdenator = $repo->findNewOrdenator($bannerPromotion->getId(), -1);
                    break;
                default:
                    throw new Exception('Invalid argument for direction.', 1);
                    break;
            }

            $target = $repo->findOneByOrdenator($newOrdenator);

            $target->setOrdenator($previousOrdenator);
            $bannerPromotion->setOrdenator($newOrdenator);

            $em->persist($target);
            $em->persist($bannerPromotion);

            $em->flush();

            $view = $this->view($bannerPromotion, 201);
        } catch (\Exception $ex) {
            $view = $this->view([
                'code' => 500,
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage()
            ], 500);
        }

        return $this->handleView($view);
    }

    /**
     * @Post("/{id}/isactive/{status}",
     *      name="api_bannerpromotion_active",
     *      defaults={ "_format" = "json" },
     *      requirements={ "id": "\d+", "status": "1|0" })
     */
    public function postBannerPromotionActive(BannerPromotion $bannerPromotion, $status)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $context = new Context();

            $bannerPromotion->setIsActive($status);

            $em->persist($bannerPromotion);
            $em->flush();

            $view = $this->view($bannerPromotion, 201);
        } catch (\Exception $ex) {
            $view = $this->view([
                'code' => 500,
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage()
            ], 500);
        }

        return $this->handleView($view);
    }
}
