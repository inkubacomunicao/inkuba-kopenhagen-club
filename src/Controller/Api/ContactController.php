<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * Contact controller.
 *
 * @Route("api/contact")
 */
class ContactController extends AbstractFOSRestController
{
    private $multidadosBaseUrl;
    private $multidadosUsername;
    private $multidadosPassword;

    /**
     * Constructor
     *
     * @param String $multidadosBaseUrl
     * @param String $multidadosUsername
     * @param String $multidadosPassword
     */
    public function __construct(String $multidadosBaseUrl, String $multidadosUsername, String $multidadosPassword)
    {
        $this->multidadosBaseUrl = $multidadosBaseUrl;
        $this->multidadosUsername = $multidadosUsername;
        $this->multidadosPassword = $multidadosPassword;
    }

     /**
      * @Post("",
      *      name="api_contact_create",
      *      defaults={ "_format" = "json" })
      *
      * @param Request $request
      *
      * @return Response
      */
    public function postContactAction(Request $request) : Response
    {
        try {
            $content = json_decode($request->getContent(), true);

            $result = $this->sendTicket($content);

            $view = $this->view(['ticket' => $result], 200);
        } catch (\Exception $ex) {
            $view = $this->view([
                'message' => 'Internal Server Error',
                'exception' => $ex->getMessage(),
            ], 500);
        }

        return $this->handleView($view);
    }

    /**
     * Function sendTicket
     *
     * @param Array $content
     *
     * @return String
     */
    private function sendTicket(Array $content) : String
    {
        switch ($content['subject']) {
            case 'reclamacao':
                $divisao = 'RKOPCLUB';
                $solicitacao = 'RKC';
                break;
            case 'informacao':
                $divisao = 'IKOPCLUB';
                $solicitacao = 'IKC';
                break;
            case 'solicitacao':
                $divisao = 'SKOPCLUB';
                $solicitacao = 'SKC';
                break;
            case 'elogio':
                $divisao = 'EKOPCLUB';
                $solicitacao = 'EKC';
                break;
            case 'sugestao':
                $divisao = 'SGTKOPCLUB';
                $solicitacao = 'SGTKC';
                break;
            default:
                throw new \Exception('Assunto inválido.');
        }

        preg_match('/\((\d{2})\) (\d+)-?(\d+)?/', $content['celular'], $matches);

        if (count($matches)) {
            $ddd = $matches[1];
            $celular = count($matches) > 3 ? $matches[2].$matches[3] : $matches[2];
        } else {
            $ddd = null;
            $celular = null;
        }

        $url = sprintf(
            '/webservices/rest/api.php?api_method=incluir_oc&usuario_ws=%s&senha_ws=%s&cod_divisao=%s&cod_solicitacao=%s&campos_variaveis_clientes=%s&nome_cliente=%s&ddd_cel_cliente=%s&tel_cel_cliente=%s&email_cliente=%s&descricao=%s',
            urlencode($this->multidadosUsername),
            urlencode($this->multidadosPassword),
            urlencode($divisao),
            urlencode($solicitacao),
            urlencode("[{\"codigo\":\"CPFFORM\",\"valor\":\"".$content['cpf']."\"}]"),
            urlencode(trim($content['name'].' '.$content['surname'])),
            urlencode($ddd),
            urlencode($celular),
            urlencode($content['email']),
            urlencode($content['description'])
        );

        $response = $this->fetch([
            'url' => $url,
            'method' => 'POST',
        ]);

        if ($response && $response['info'] && $response['info']['http_code'] && $response['info']['http_code'] === 200) {
            if (!is_array($response['result']) && str_starts_with($response['result'], 'F')) {
                if (str_contains($response['result'], 'Ja existe uma ocorrencia')) {
                    return $response['result'];
                }

                throw new \Exception($response['result']);
            }

            if (!is_array($response['result']) && str_starts_with($response['result'], 'T')) {
                return $response['result'];
            }
        }

        throw new \Exception(sprintf('Resposta inválida do serviço de chamados: %s', json_encode($response['result'])));
    }

    /**
     * Function fetch
     *
     * @param Array $params
     *
     * @return Array
     */
    private function fetch(Array $params) : Array
    {
        if (!$params || !is_array($params)) {
            throw new \Exception('Request params missing.', 1);
        }

        if (!$this->multidadosBaseUrl) {
            throw new \Exception('Base API URL not set-up.', 1);
        }

        if (!array_key_exists('url', $params)) {
            throw new \Exception('URL is missing', 1);
        }

        if (!array_key_exists('method', $params)) {
            $params['method'] = 'GET';
        } else {
            $params['method'] = strtoupper($params['method']);
        }

        $headers = [
            'Content-Type: application/json',
        ];

        $curl = curl_init($this->multidadosBaseUrl.$params['url']);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $params['method']);

        if (array_key_exists('token', $params)) {
            array_push($headers, 'Authorization: '.$params['token']);
        }

        if (array_key_exists('json', $params)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params['json']);
            array_push($headers, 'Content-Length: '.strlen($params['json']));
        } else {
            array_push($headers, 'Content-Length: 0');
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $raw = curl_exec($curl);
        $info = curl_getinfo($curl);
        $result = json_decode($raw, true);

        $response = [
            'info' => $info,
            'result' => $result,
        ];

        return $response;
    }
}
