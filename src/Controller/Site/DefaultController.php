<?php

namespace App\Controller\Site;

use App\Entity\Promotion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="site_home")
     *
     * @return Response
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->enable('displayable_data');

        $banners = $em->getRepository('App:BannerHome')->findBy([], ['ordenator' => 'DESC']);

        return $this->render('site/index.html.twig', [
            'banners' => $banners,
        ]);
    }

    /**
     * @Route("/participe", name="site_participate")
     *
     * @return Response
     */
    public function participate(): Response
    {
        return $this->render('site/participate.html.twig');
    }

    /**
     * @Route("/callback", name="site_callback")
     *
     * @return Response
     */
    public function callback(): Response
    {
        return $this->render('site/callback.html.twig');
    }

    /**
     * @Route("/sobre", name="site_about")
     *
     * @return Response
     */
    public function about(): Response
    {
        return $this->render('site/about.html.twig');
    }

    /**
     * @Route("/meus-pontos", name="site_points")
     *
     * @return Response
     */
    public function myPoints(): Response
    {
        return $this->render('site/my-points.html.twig');
    }

    /**
     * @Route("/meus-vouchers", name="site_vouchers")
     *
     * @return Response
     */
    public function myVouchers(): Response
    {
        return $this->render('site/my-vouchers.html.twig');
    }

    /**
     * @Route("/como-participar", name="site_howto")
     *
     * @return Response
     */
    public function howto(): Response
    {
        return $this->render('site/howto.html.twig');
    }

    /**
     * @Route("/regulamento", name="site_legal")
     *
     * @return Response
     */
    public function legal(): Response
    {
        return $this->render('site/legal.html.twig');
    }

    /**
     * @Route("/politicas-de-privacidade", name="site_privacy")
     *
     * @return Response
     */
    public function privacy(): Response
    {
        return $this->render('site/privacy.html.twig');
    }

    /**
     * @Route("/politicas-de-cookies", name="site_cookie")
     *
     * @return Response
     */
    public function cookies(): Response
    {
        return $this->render('site/cookies.html.twig');
    }

    /**
     * @Route("/contato", name="site_contact")
     *
     * @return Response
     */
    public function contact(): Response
    {
        return $this->render('site/contact.html.twig');
    }

    /**
     * @Route("/{slug}", name="site_promotion")
     *
     * @param Promotion $promotion
     *
     * @return Response
     */
    public function promotion(Promotion $promotion): Response
    {
        return $this->render('site/promotion.html.twig', [
            'promotion' => $promotion,
        ]);
    }
}
