<?php

namespace App\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;

/**
 * Class BannerPromotionDatatable.
 */
class BannerPromotionDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'language_by_locale' => true,
        ));

        $this->ajax->set(array(
            'method' => 'POST',
        ));

        $this->options->set(array(
            'dom' => 't',
            'order' => array(array(3, 'desc')),
            'individual_filtering' => false,
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'page_length' => null,
        ));

        $this->features->set(array(
        ));

        $this->maxOrdernator = $this->em
                                    ->getRepository('App:BannerPromotion')
                                    ->findMaxOrdenator($options['promotionId']);

        $this->columnBuilder
            ->add('id', Column::class, array(
                'title' => $this->translator->trans('cms.field.id', [], 'cms'),
                'width' => '20px',
                'orderable' => false,
            ))
            ->add('image', ImageColumn::class, array(
                'title' => $this->translator->trans('cms.field.image', [], 'cms'),
                'width' => '100px',
                'imagine_filter' => 'cms_datatable_thumb',
                'imagine_filter_enlarged' => 'cms_datatable_preview',
                'relative_path' => $options['imagePath'],
                'holder_url' => 'http://via.placeholder.com/80x45',
                'enlarge' => true,
                'orderable' => false,
                'searchable' => false,
            ))
            ->add('imageMobile', ImageColumn::class, array(
                'title' => $this->translator->trans('cms.field.imageMobile', [], 'cms'),
                'width' => '100px',
                'imagine_filter' => 'cms_datatable_thumb',
                'imagine_filter_enlarged' => 'cms_datatable_preview',
                'relative_path' => $options['imagePath'],
                'holder_url' => 'http://via.placeholder.com/80x45',
                'enlarge' => true,
                'orderable' => false,
                'searchable' => false,
            ))
            ->add('title', Column::class, array(
                'title' => $this->translator->trans('cms.field.title', [], 'cms'),
                'orderable' => false,
                'filter' => [TextFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add('ordenator', Column::class, array(
                'title' => $this->translator->trans('cms.field.ordenator', [], 'cms'),
                'width' => '20px',
                'orderable' => true,
            ))
            ->add('isActive', BooleanColumn::class, array(
                'title' => $this->translator->trans('cms.field.is_active', [], 'cms'),
                'width' => '60px',
                'orderable' => false,
                'true_label' => $this->translator->trans('cms.datatable.yes', [], 'cms'),
                'false_label' => $this->translator->trans('cms.datatable.no', [], 'cms'),
                'filter' => [SelectFilter::class, [
                    'search_type' => 'eq',
                    'select_options' => [
                        '' => $this->translator->trans('cms.datatable.any', [], 'cms'),
                        'true' => $this->translator->trans('cms.datatable.yes', [], 'cms'),
                        'false' => $this->translator->trans('cms.datatable.no', [], 'cms'),
                    ],
                    'cancel_button' => false,
                ]],
            ))
            ->add('createdAt', DateTimeColumn::class, array(
                'title' => $this->translator->trans('cms.field.created_at', [], 'cms'),
                'width' => '120px',
                'orderable' => false,
                'date_format' => 'DD-MM-Y HH:mm:ss',
                'filter' => [DateRangeFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'width' => '140px',
                'class_name' => 'text-center',
                'actions' => array(
                    array(
                        'route' => 'api_bannerpromotion_ordenator',
                        'route_parameters' => array(
                            'id' => 'id',
                            'direction' => 'up'
                        ),
                        'icon' => 'glyphicon glyphicon-arrow-up',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.move_up', [], 'cms'),
                            'class' => 'btn btn-primary btn-xs ordenator',
                            'role' => 'button',
                            'data-error-message' => 'cms.message.operation_error'
                        ),
                        'render_if' => function ($row) {
                            return $row['ordenator'] < $this->maxOrdernator;
                        },
                    ),
                    array(
                        'icon' => 'glyphicon glyphicon-arrow-up',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.move_up', [], 'cms'),
                            'class' => 'btn disabled btn-primary btn-xs ordenator',
                            'role' => 'button',
                        ),
                        'render_if' => function ($row) {
                            return $row['ordenator'] >= $this->maxOrdernator;
                        },
                    ),
                    array(
                        'icon' => 'glyphicon glyphicon-arrow-down',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.move_down', [], 'cms'),
                            'class' => 'btn disabled btn-primary btn-xs ordenator',
                            'role' => 'button',
                        ),
                        'render_if' => function ($row) {
                            return $row['ordenator'] <= 1;
                        },
                    ),
                    array(
                        'route' => 'api_bannerpromotion_ordenator',
                        'route_parameters' => array(
                            'id' => 'id',
                            'direction' => 'down'
                        ),
                        'icon' => 'glyphicon glyphicon-arrow-down',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.move_down', [], 'cms'),
                            'class' => 'btn btn-primary btn-xs ordenator',
                            'role' => 'button',
                            'data-direction' => 'down',
                            'data-error-message' => 'cms.message.operation_error'
                        ),
                        'render_if' => function ($row) {
                            return $row['ordenator'] > 1;
                        },
                    ),
                    array(
                        'route' => 'api_bannerpromotion_active',
                        'route_parameters' => array(
                            'id' => 'id',
                            'status' => '1'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-close',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.inactive', [], 'cms'),
                            'class' => 'btn btn-danger btn-xs status',
                            'role' => 'button',
                            'data-error-message' => 'cms.message.operation_error',
                            'data-confirm-message' => 'cms.message.confirm_activate'
                        ),
                        'render_if' => function ($row) {
                            return boolval(preg_match(
                                '/'.$this->translator->trans('cms.datatable.no', [], 'cms').'/',
                                $row['isActive'],
                                $matches
                            ));
                        },
                    ),
                    array(
                        'route' => 'api_bannerpromotion_active',
                        'route_parameters' => array(
                            'id' => 'id',
                            'status' => '0'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.active', [], 'cms'),
                            'class' => 'btn btn-success btn-xs status',
                            'role' => 'button',
                            'data-error-message' => 'cms.message.operation_error',
                            'data-confirm-message' => 'cms.message.confirm_inactivate'
                        ),
                        'render_if' => function ($row) {
                            return boolval(preg_match(
                                '/'.$this->translator->trans('cms.datatable.yes', [], 'cms').'/',
                                $row['isActive'],
                                $matches
                            ));
                        },
                    ),
                    array(
                        'route' => 'cms_bannerpromotion_show',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'glyphicon glyphicon-sunglasses',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.show'),
                            'class' => 'btn btn-default btn-xs',
                            'role' => 'button',
                        ),
                    ),
                    array(
                        'route' => 'cms_bannerpromotion_edit',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button',
                        ),
                    ),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\BannerPromotion';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'bannerPromotion_datatable';
    }
}
