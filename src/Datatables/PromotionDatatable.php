<?php

namespace App\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;

/**
 * Class PromotionDatatable.
 */
class PromotionDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'language_by_locale' => true,
        ));

        $this->ajax->set(array(
            'method' => 'POST',
        ));

        $this->options->set(array(
            'order' => array(array(0, 'desc')),
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => Style::BOOTSTRAP_3_STYLE,
            'search_in_non_visible_columns' => true
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
                'title' => $this->translator->trans('cms.field.id', [], 'cms'),
                'width' => '40px',
                'orderable' => true,
                'filter' => [TextFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add('title', Column::class, array(
                'title' => $this->translator->trans('cms.field.title', [], 'cms'),
                'orderable' => true,
                'filter' => [TextFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add('slug', Column::class, array(
                'title' => $this->translator->trans('cms.field.slug', [], 'cms'),
                'orderable' => true,
                'filter' => [TextFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add('description', Column::class, array(
                'title' => $this->translator->trans('cms.field.description', [], 'cms'),
                'visible' => false,
                'searchable' => true
            ))
            ->add('isActive', BooleanColumn::class, array(
                'title' => $this->translator->trans('cms.field.is_active', [], 'cms'),
                'width' => '60px',
                'orderable' => false,
                'true_label' => $this->translator->trans('cms.datatable.yes', [], 'cms'),
                'false_label' => $this->translator->trans('cms.datatable.no', [], 'cms'),
                'filter' => [SelectFilter::class, [
                    'search_type' => 'eq',
                    'select_options' => [
                        '' => $this->translator->trans('cms.datatable.any', [], 'cms'),
                        'true' => $this->translator->trans('cms.datatable.yes', [], 'cms'),
                        'false' => $this->translator->trans('cms.datatable.no', [], 'cms'),
                    ],
                    'cancel_button' => false,
                ]],
            ))
            ->add('publishedAt', DateTimeColumn::class, array(
                'title' => $this->translator->trans('cms.field.published_at', [], 'cms'),
                'width' => '120px',
                'orderable' => true,
                'date_format' => 'DD-MM-Y HH:mm:ss',
                'filter' => [DateRangeFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add('expiredAt', DateTimeColumn::class, array(
                'title' => $this->translator->trans('cms.field.expired_at', [], 'cms'),
                'width' => '120px',
                'orderable' => true,
                'date_format' => 'DD-MM-Y HH:mm:ss',
                'default_content' => $this->translator->trans('cms.message.never', [], 'cms'),
                'filter' => [DateRangeFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add('createdAt', DateTimeColumn::class, array(
                'title' => $this->translator->trans('cms.field.created_at', [], 'cms'),
                'width' => '120px',
                'orderable' => true,
                'date_format' => 'DD-MM-Y HH:mm:ss',
                'filter' => [DateRangeFilter::class, [
                    'placeholder_text' => $this->translator->trans('cms.datatable.type_to_filter', [], 'cms'),
                    'cancel_button' => true,
                ]],
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'width' => '100px',
                'class_name' => 'text-center',
                'actions' => array(
                    array(
                        'route' => 'api_promotion_active',
                        'route_parameters' => array(
                            'id' => 'id',
                            'status' => '1'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-close',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.inactive', [], 'cms'),
                            'class' => 'btn btn-danger btn-xs status',
                            'role' => 'button',
                            'data-error-message' => 'cms.message.operation_error',
                            'data-confirm-message' => 'cms.message.confirm_activate'
                        ),
                        'render_if' => function ($row) {
                            return boolval(preg_match(
                                '/'.$this->translator->trans('cms.datatable.no', [], 'cms').'/',
                                $row['isActive'],
                                $matches
                            ));
                        },
                    ),
                    array(
                        'route' => 'api_promotion_active',
                        'route_parameters' => array(
                            'id' => 'id',
                            'status' => '0'
                        ),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('cms.button.active', [], 'cms'),
                            'class' => 'btn btn-success btn-xs status',
                            'role' => 'button',
                            'data-error-message' => 'cms.message.operation_error',
                            'data-confirm-message' => 'cms.message.confirm_inactivate'
                        ),
                        'render_if' => function ($row) {
                            return boolval(preg_match(
                                '/'.$this->translator->trans('cms.datatable.yes', [], 'cms').'/',
                                $row['isActive'],
                                $matches
                            ));
                        },
                    ),
                    array(
                        'route' => 'cms_promotion_show',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'glyphicon glyphicon-sunglasses',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.show'),
                            'class' => 'btn btn-default btn-xs',
                            'role' => 'button',
                        ),
                    ),
                    array(
                        'route' => 'cms_promotion_edit',
                        'route_parameters' => array(
                            'id' => 'id',
                        ),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button',
                        ),
                    ),
                ),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\Promotion';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'cms_promotion_datatable';
    }
}
