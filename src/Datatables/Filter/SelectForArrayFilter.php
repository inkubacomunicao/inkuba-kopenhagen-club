<?php

/**
 * This file is part of the SgDatatablesBundle package.
 *
 * (c) stwe <https://github.com/stwe/DatatablesBundle>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Datatables\Filter;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Andx;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Doctrine\ORM\Query\Expr\Composite;
use Exception;

/**
 * Class SelectForArrayFilter
 *
 */
class SelectForArrayFilter extends SelectFilter
{
    /**
     * Get an expression.
     *
     * @param Composite    $expr
     * @param QueryBuilder $qb
     * @param string       $searchType
     * @param string       $searchField
     * @param mixed        $searchValue
     * @param string       $searchTypeOfField
     * @param int          $parameterCounter
     *
     * @return Composite
     */
    protected function getExpression(Composite $expr, QueryBuilder $qb, $searchType, $searchField, $searchValue, $searchTypeOfField, &$parameterCounter)
    {
        // Prevent doctrine issue with "?0" (https://github.com/doctrine/doctrine2/issues/6699)
        $parameterCounter++;

        // Only StringExpression can be searched with LIKE (https://github.com/doctrine/doctrine2/issues/6363)
        if (!preg_match('/text|string|array|date|time/', $searchTypeOfField) // Not a StringExpression
            || preg_match('/SELECT.+FROM.+/is', $searchField) // Subqueries can't be search with LIKE
            || preg_match('/CASE.+WHEN.+END/is', $searchField) // CASE WHEN can't be search with LIKE
        ) {
            switch ($searchType) {
                case 'like':
                    $searchType = 'eq';
                    break;
                case 'notLike':
                    $searchType = 'neq';
                    break;
            }
        }

        // Skip search on columns when column type don't match search value type
        // (Prevent converting data type errors)
        $incompatibleTypeOfField = false;
        switch ($searchTypeOfField) {
            case 'decimal':
            case 'float':
                if (is_numeric($searchValue)) {
                    $searchValue = (float) $searchValue;
                } else {
                    $incompatibleTypeOfField = true;
                }
                break;
            case 'integer':
            case 'bigint':
            case 'smallint':
            case 'boolean':
                if ($searchValue == (string) (int) $searchValue) {
                    $searchValue = (int) $searchValue;
                } else {
                    $incompatibleTypeOfField = true;
                }
                break;
        }
        if ($incompatibleTypeOfField) {
            return
                $expr instanceof Andx
                    // No result found
                    ? $expr->add($qb->expr()->eq(1, 0))
                    // Column skipped from search
                    : $expr
                ;
        }

        switch ($searchType) {
            case 'like':
                $expr->add($qb->expr()->like($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, '%'.$searchValue.'%');
                break;
            case '%like':
                $expr->add($qb->expr()->like($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, '%'.$searchValue);
                break;
            case 'like%':
                $expr->add($qb->expr()->like($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue.'%');
                break;
            case 'notLike':
                $expr->add($qb->expr()->notLike($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, '%'.$searchValue.'%');
                break;
            case 'eq':
                $expr->add($qb->expr()->eq($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue);
                break;
            case 'neq':
                $expr->add($qb->expr()->neq($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue);
                break;
            case 'lt':
                $expr->add($qb->expr()->lt($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue);
                break;
            case 'lte':
                $expr->add($qb->expr()->lte($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue);
                break;
            case 'gt':
                $expr->add($qb->expr()->gt($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue);
                break;
            case 'gte':
                $expr->add($qb->expr()->gte($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, $searchValue);
                break;
            case 'in':
                $expr->add($qb->expr()->in($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, explode(',', $searchValue));
                break;
            case 'notIn':
                $expr->add($qb->expr()->notIn($searchField, '?'.$parameterCounter));
                $qb->setParameter($parameterCounter, explode(',', $searchValue));
                break;
            case 'isNull':
                $expr->add($qb->expr()->isNull($searchField));
                break;
            case 'isNotNull':
                $expr->add($qb->expr()->isNotNull($searchField));
                break;
        }

        return $expr;
    }
}
