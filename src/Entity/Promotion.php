<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Promotion.
 *
 * @ORM\Table(
 *      schema="site",
 *      name="promotions",
 *      indexes={
 *          @ORM\Index(name="promotions_slug_idx", columns={"slug"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\PromotionRepository")
 * @UniqueEntity(
 *     fields={"slug"},
 *     errorPath="slug",
 *     message="promotion.slug.unique"
 * )
 */
class Promotion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=false)
     * @Assert\NotBlank(
     *      message = "promotion.title.not_blank"
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=272, nullable=false)
     * @Assert\Regex(
     *     pattern="/^[a-z0-9\-]+$/",
     *     match=true,
     *     message="promotion.slug.valid"
     * )
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2048, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     * @Assert\NotBlank(
     *      message = "promotion.content.not_blank"
     * )
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="regulation", type="text", nullable=true)
     */
    private $regulation;

    /**
     * @ORM\OneToMany(targetEntity="BannerPromotion", mappedBy="promotion")
     */
    private $banners;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=false)
     */
    private $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     * @Assert\Expression(
     *     "this.getExpiredAt() == null || this.getPublishedAt() < this.getExpiredAt()",
     *     message="promotion.expiredAt.gt.publishedAt"
     * )
     */
    private $expiredAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->publishedAt = new \Datetime();
        $this->expiredAt = null;
        $this->createdAt = new \Datetime();
        $this->banners = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Promotion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Promotion
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Promotion
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return Promotion
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set regulation.
     *
     * @param string|null $regulation
     *
     * @return Promotion
     */
    public function setRegulation($regulation = null)
    {
        $this->regulation = $regulation;

        return $this;
    }

    /**
     * Get regulation.
     *
     * @return string|null
     */
    public function getRegulation()
    {
        return $this->regulation;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return Promotion
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set publishedAt.
     *
     * @param \DateTime $publishedAt
     *
     * @return Promotion
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt.
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set expiredAt.
     *
     * @param \DateTime $expiredAt
     *
     * @return Promotion
     */
    public function setExpiredAt($expiredAt)
    {
        if ($expiredAt instanceof \DateTime) {
            $this->expiredAt = $expiredAt;
        } else {
            if (null != $expiredAt) {
                $this->expiredAt = new \DateTime($expiredAt);
            }
        }

        return $this;
    }

    /**
     * Get expiredAt.
     *
     * @return \DateTime
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Promotion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add banner.
     *
     * @param \App\Entity\BannerPromotion $banner
     *
     * @return Promotion
     */
    public function addBanner(\App\Entity\BannerPromotion $banner)
    {
        $this->banners[] = $banner;

        return $this;
    }

    /**
     * Remove banner.
     *
     * @param \App\Entity\BannerPromotion $banner
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBanner(\App\Entity\BannerPromotion $banner)
    {
        return $this->banners->removeElement($banner);
    }

    /**
     * Get banners.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanners()
    {
        return $this->banners;
    }
}
