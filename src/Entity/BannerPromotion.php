<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BannerPromotion
 *
 * @ORM\Table(schema="site", name="banner_promotion")
 * @ORM\Entity(repositoryClass="App\Repository\BannerPromotionRepository")
 */
class BannerPromotion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="button", type="string", length=32, nullable=true)
     */
    private $button;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=1920, maxWidth=2560, minHeight=300, maxHeight=1444)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="image_mobile", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=640, maxWidth=1080, minHeight=900, maxHeight=1920)
     */
    private $imageMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1024, nullable=true)
     * @Assert\Expression(
     *     "!this.getButton() || (!!value)",
     *     message="cms.bannerpromotion.url.mandatory_if_button"
     * )
     * @Assert\Url(
     *      message = "cms.bannerpromotion.url.invalid",
     *      protocols = {"http", "https"},
     *      checkDNS = "ANY",
     *      dnsMessage = "cms.bannerpromotion.url.unresolved"
     * )
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="Promotion", inversedBy="banners")
     * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id", nullable=false)
     */
    private $promotion;

    /**
     * @var int
     *
     * @ORM\Column(name="ordenator", type="integer")
     */
    private $ordenator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return BannerPromotion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set image.
     *
     * @param string|null $image
     *
     * @return BannerPromotion
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set imageMobile.
     *
     * @param string $imageMobile
     *
     * @return BannerPromotion
     */
    public function setImageMobile($imageMobile)
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    /**
     * Get imageMobile.
     *
     * @return string
     */
    public function getImageMobile()
    {
        return $this->imageMobile;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return BannerPromotion
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set ordenator.
     *
     * @param int $ordenator
     *
     * @return BannerPromotion
     */
    public function setOrdenator($ordenator)
    {
        $this->ordenator = $ordenator;

        return $this;
    }

    /**
     * Get ordenator.
     *
     * @return int
     */
    public function getOrdenator()
    {
        return $this->ordenator;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return BannerPromotion
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return BannerPromotion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set promotion.
     *
     * @param \App\Entity\Promotion $promotion
     *
     * @return BannerPromotion
     */
    public function setPromotion(\App\Entity\Promotion $promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

    /**
     * Get promotion.
     *
     * @return \App\Entity\Promotion
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return BannerPromotion
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set button.
     *
     * @param string|null $button
     *
     * @return BannerPromotion
     */
    public function setButton($button = null)
    {
        $this->button = $button;

        return $this;
    }

    /**
     * Get button.
     *
     * @return string|null
     */
    public function getButton()
    {
        return $this->button;
    }
}
