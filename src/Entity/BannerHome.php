<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BannerHome
 *
 * @ORM\Table(schema="site", name="banner_home")
 * @ORM\Entity(repositoryClass="App\Repository\BannerHomeRepository")
 */
class BannerHome
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=64, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="button", type="string", length=32, nullable=true)
     */
    private $button;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=1920, maxWidth=2560, minHeight=300, maxHeight=1444)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="image_mobile", type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg", "image/jpg", "image/bmp", "image/tiff" })
     * @Assert\Image(minWidth=640, maxWidth=1080, minHeight=900, maxHeight=1920)
     */
    private $imageMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1024, nullable=true)
     * @Assert\Expression(
     *     "!this.getButton() || (!!value)",
     *     message="cms.bannerhome.url.mandatory_if_button"
     * )
     * @Assert\Url(
     *      message = "cms.bannerhome.url.invalid",
     *      protocols = {"http", "https"},
     *      checkDNS = "ANY",
     *      dnsMessage = "cms.bannerhome.url.unresolved"
     * )
     */
    private $url;

    /**
     * @var int
     *
     * @ORM\Column(name="ordenator", type="integer")
     */
    private $ordenator;

    /**
     * @var string
     *
     * @ORM\Column(name="audience", type="array", length=64, nullable=true)
     */
    private $audience;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=false)
     */
    private $publishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     * @Assert\Expression(
     *     "this.getExpiredAt() == null || this.getPublishedAt() < this.getExpiredAt()",
     *     message="cms.bannerHome.expiredAt.gt.publishedAt"
     * )
     */
    private $expiredAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->audience = ['authenticated', 'anonymous'];
        $this->publishedAt = new \Datetime();
        $this->expiredAt = null;
        $this->createdAt = new \Datetime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @return BannerHome
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return BannerHome
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return BannerHome
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set imageMobile.
     *
     * @param string $imageMobile
     *
     * @return BannerHome
     */
    public function setImageMobile($imageMobile)
    {
        $this->imageMobile = $imageMobile;

        return $this;
    }

    /**
     * Get imageMobile.
     *
     * @return string
     */
    public function getImageMobile()
    {
        return $this->imageMobile;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return BannerHome
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set audience.
     *
     * @param string $audience
     *
     * @return BannerHome
     */
    public function setAudience($audience)
    {
        $this->audience = $audience;

        return $this;
    }

    /**
     * Get audience.
     *
     * @return string
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * Get ordenator.
     *
     * @return int
     */
    public function getOrdenator()
    {
        return $this->ordenator;
    }

    /**
     * Set ordenator.
     *
     * @return BannerHome
     */
    public function setOrdenator($ordenator)
    {
        $this->ordenator = $ordenator;

        return $this;
    }

    /**
     * Set expiredAt.
     *
     * @param \DateTime $expiredAt
     *
     * @return BannerHome
     */
    public function setExpiredAt($expiredAt)
    {
        if ($expiredAt instanceof \DateTime) {
            $this->expiredAt = $expiredAt;
        } else {
            if (null != $expiredAt) {
                $this->expiredAt = new \DateTime($expiredAt);
            }
        }

        return $this;
    }

    /**
     * Get expiredAt.
     *
     * @return \DateTime
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Get the value of publishedAt.
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set the value of publishedAt.
     *
     * @param \DateTime $publishedAt
     *
     * @return self
     */
    public function setPublishedAt(\DateTime $publishedAt)
    {
        if ($publishedAt instanceof \DateTime) {
            $this->publishedAt = $publishedAt;
        } else {
            $this->publishedAt = new \DateTime($publishedAt);
        }

        return $this;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return BannerHome
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Publication
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return BannerHome
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set button.
     *
     * @param string|null $button
     *
     * @return BannerHome
     */
    public function setButton($button = null)
    {
        $this->button = $button;

        return $this;
    }

    /**
     * Get button.
     *
     * @return string|null
     */
    public function getButton()
    {
        return $this->button;
    }
}
