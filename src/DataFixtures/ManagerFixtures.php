<?php

namespace App\DataFixtures;

use App\Entity\Manager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class ManagerFixtures
 */
class ManagerFixtures extends Fixture
{
    private $encoder;

    /**
     * Constructor
     *
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * Function load
     *
     * @param ObjectManager $om
     *
     * @return void
     */
    public function load(ObjectManager $om)
    {
        $manager = new Manager();

        $manager->setUsername('manager@kopclub.com.br');
        $manager->setRoles(['ROLE_MANAGER']);
        $manager->setIsActive(true);
        $manager->setCreatedAt(new \DateTime());
        $manager->setPassword($this->encoder->encodePassword($manager, 'BsNwbsTKbLBpyJ5OdsZ6boXhQxcxoqPH'));

        $om->persist($manager);
        $om->flush();
    }
}
