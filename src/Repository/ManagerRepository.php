<?php

namespace App\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * ManagerRepository
 */
class ManagerRepository extends \Doctrine\ORM\EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->getEntityManager()
                    ->createQuery('
                        SELECT u
                        FROM App\Entity\Manager u
                        WHERE
                            u.username = :username AND
                            u.isActive = 1')
                    ->setParameter('username', $username)
                    ->getOneOrNullResult();
    }

    public function findByRole($role)
    {
        return $this->getEntityManager()
                    ->createQuery('
                        SELECT u
                        FROM App\Entity\Manager u
                        WHERE
                            u.roles LIKE :role
                        ORDER BY u.username ASC')
                    ->setParameter('role', "%$role%")
                    ->getResult();

    }
}
