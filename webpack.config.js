var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    .addEntry('site_general', './assets/scripts/site/general.js')
    .addEntry('site_landing', './assets/scripts/site/landing.js')
    .addEntry('site_home', './assets/scripts/site/home.js')
    .addEntry('site_callback', './assets/scripts/site/callback.js')
    .addEntry('site_promotion', './assets/scripts/site/promotion.js')
    .addEntry('site_points', './assets/scripts/site/points.js')
    .addEntry('site_vouchers', './assets/scripts/site/vouchers.js')
    .addEntry('site_contact', './assets/scripts/site/contact.js')
    .addEntry('site_success', './assets/scripts/site/success.js')
    .addEntry('cms_general', './assets/scripts/cms/general.js')
    .addEntry('cms_login', './assets/scripts/cms/login.js')
    .addEntry('cms_promotion', './assets/scripts/cms/promotion.js')
    .addEntry('cms_bannerhome', './assets/scripts/cms/bannerhome.js')
    .addEntry('cms_manager', './assets/scripts/cms/manager.js')


    // you could add standalone style files, but you shouldn't
    //.addStyleEntry('app', './assets/scss/some_page.css')

    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })


    .autoProvideVariables({
        '$' : 'jquery',
        'jQuery' : 'jquery',
        'global.$' : 'jquery',
        'global.jQuery' : 'jquery',
        'moment' : 'moment',
        'global.moment' : 'moment',
        'Quill' : 'quill',
        'global.Quill' : 'quill',
        'window.Quill' : 'quill',
    })

    .enableSassLoader()
    .enablePostCssLoader()
    .enableIntegrityHashes(Encore.isProduction())
    // .autoProvidejQuery()

    .copyFiles({
        from: './assets/images',
        to: 'images/[path][name].[hash:8].[ext]'
    })
;

module.exports = Encore.getWebpackConfig();
