inkuba-kopenhagen-club
=========================

A Symfony project created on July 22, 2019, 10:56 am.

Requires:
- PHP 8.0.x
- Composer
- NodeJS 14.17.6 (LTS)
- NPM

Install
====
**Run**: npm install (development only)

**Run**: composer install

Setup your .env accordingly.

Run
====

Server runs on 127.0.0.1:8000 by default and is proxied by browser-sync on 127.0.0.1:3000, you will access via the last one.

All assets are managed by Webpack Encore, SCSS are pre-compiled and post-compiled with autoprefixer, JavaScript is transpiled and packed by Webpack, images are pass-trough the destination folder (could be processed using webpack).

Live reload is enabled. If you modify and of the package or config files in the root directory, you must reload the server.

**Run**: npm start (development only)

**Clear**: php bin/console cache:clear --env=prod or dev

Build
===

**Run**: npm run encore:build:prd
