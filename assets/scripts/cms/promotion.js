import Binder from './libs/binder';
import Editor from './libs/editor';
import Datatables from './libs/datatables';

import '../../sass/cms/promotion.scss';

$(document).ready(function() {
    Binder.bindSlugify('#appbundle_promotion_title', '#appbundle_promotion_slug');
    Editor.bindEditor('form[name=appbundle_promotion]', ['#appbundle_promotion_content', '#appbundle_promotion_regulation']);
    Datatables.bindDatatableActions();
});
