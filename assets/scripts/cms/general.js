import * as $ from 'jquery';
import * as moment from 'moment';
import Quill from 'quill';
import Binder from './libs/binder';

import 'bootstrap';
import 'admin-lte';

import * as dt from 'datatables.net';
import 'datatables.net-bs/js/dataTables.bootstrap.js';
import 'datatables.net-bs/css/dataTables.bootstrap.css';

import 'ionicons/dist/css/ionicons.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/skin-yellow.min.css';
import '../../sass/cms/general.scss';

window.$ = window.jQuery = global.$ = global.jQuery = $;
window.moment = global.moment = moment;
window.Quill = global.Quill = Quill;

console.log('Read', $);

$(document).ready(function(){
    Binder.bindConfirmDangerousActions();
    Binder.bindFeatherlight();
    Binder.bindSelect2();
    //tableInit();
});
