import Datatables from './libs/datatables';

$(document).ready(function() {
    Datatables.bindDatatableActions();
});
