import $ from 'jquery';
import slugify from 'slugify';
import 'featherlight';
import 'select2';

import 'select2/dist/css/select2.min.css';
import 'featherlight/release/featherlight.min.css';

export default class {
    static bindSlugify(input, output) {
        const i = input instanceof jQuery ? input : $(input);
        const o = output instanceof jQuery ? output : $(output);

        if (o.length > 0 && o.val().length === 0) {
            i.on('change keyup', function(e){
                o.val(slugify(i.val(), {
                    remove: /[^a-zA-Z0-9\-\s]/g,
                    replacement: '-',
                    lower: true
                }));
            });

            o.on('keyup', function(){
                i.off('change keyup');
            });
        }
    }

    static bindFeatherlight(target = '[data-featherlight]') {
        const t = target instanceof jQuery ? target : $(target);

        if (t.length > 0) {
            t.featherlight();
        }
    }

    static bindSelect2(target = '.select2') {
        const t = target instanceof jQuery ? target : $(target);

        if (t.length > 0) {
            t.select2({
                maximumSelectionLength: $(this).data('maximumSelectionLength'),
                templateResult: function (option) {
                    var regex = new RegExp(/<!--\s(.*)\s-->\s(.*)/);
                    var matches = regex.exec(option.text);

                    return matches && matches.length === 3 ? matches[2] : option.text;
                },
                templateSelection: function (option) {
                    var regex = new RegExp(/<!--\s(.*)\s-->\s(.*)/);
                    var matches = regex.exec(option.text);

                    return matches && matches.length === 3 ? '<img src="' + matches[1] + '" /> ' + matches[2] : option.text;
                },
                escapeMarkup: function (m) {
                    return m;
                }
            });
        }
    }

    static bindConfirmDangerousActions(target = '.btn-danger, [data-confirm-message]') {
        const t = target instanceof jQuery ? target : $(target);

        if (t.length > 0) {
            t.on('click', function(e){
                if (!confirm($(this).data('confirm-message') || 'Are you sure you want to complete this action?')) {
                    e.preventDefault();
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    return false;
                }
            });
        }
    }
}
