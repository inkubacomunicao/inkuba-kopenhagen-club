import Quill from 'quill';
import ImageResize from 'quill-image-resize-module';

import 'quill/dist/quill.snow.css';
import '../../../sass/cms/libs/editor.scss';

class ImageFormat extends Quill.import('formats/image') {
    static formats(domNode) {
        return ['alt', 'height', 'width', 'style'].reduce(function(formats, attribute) {
        if (domNode.hasAttribute(attribute)) {
            formats[attribute] = domNode.getAttribute(attribute);
        }
        return formats;
        }, {});
    }

    format(name, value) {
        if (['alt', 'height', 'width', 'style'].indexOf(name) > -1) {
        if (value) {
            this.domNode.setAttribute(name, value);
        } else {
            this.domNode.removeAttribute(name);
        }
        } else {
        super.format(name, value);
        }
    }
}

export default class {
    static bindEditor(form, inputs) {
        const f = form instanceof jQuery ? form : $(form);
        const q = inputs.map((x) => {
            const hidden = x instanceof jQuery ? x : $(x);
            const editor = hidden.length > 0 ? $(`[data-editor-for=${hidden.attr('id')}]`) : null;

            return {
                hidden,
                editor
            };
        }).filter((x) => {
            return x.hidden.length > 0 && x.editor !== null && x.editor.length > 0;
        });

        if (f && q.length > 0) {
            Quill.register('modules/ImageResize', ImageResize);
            Quill.register(ImageFormat, true);

            const options = {
                modules: {
                    toolbar: [
                        [{ header: [3, 4, false] }],
                        ['bold', 'italic'],
                        ['blockquote', { 'align': [] }, { 'list': 'ordered'}, { 'list': 'bullet' }, 'link'],
                        ['image'],
                        ['clean']
                    ],
                    ImageResize: {
                        modules: [ 'Resize', 'DisplaySize', 'Toolbar' ]
                    },
                    clipboard: {
                        matchVisual: false
                    }
                },
                placeholder: '',
                theme: 'snow'  // or 'bubble'
            };

            q.forEach((set) => {
                const quill = new Quill(set.editor.get(0), options);

                quill.setText("");
                quill.clipboard.dangerouslyPasteHTML(0, set.hidden.val().trim().replace(/\r|\n/g, ''));

                f.on('submit', function() {
                    set.hidden.val(set.editor.children().first().html());
                    return true;
                });
            });
        }
    }
}
