import 'datatables.net-bs';
import 'featherlight';
import 'daterangepicker';
import Binder from './binder';

import 'datatables.net-bs/css/dataTables.bootstrap.min.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import '../../../sass/cms/libs/datatables.scss';

export default class {
    static bindDatatableActions() {
        const self = this;

        if($.fn.DataTable !== undefined) {
            var interval = window.setInterval(function() {
                if ($.fn.DataTable.tables().length > 0) {
                    $(window).on('draw.dt', function() {
                        Binder.bindFeatherlight();
                        Binder.bindConfirmDangerousActions();
                        self.bindStatusButtons();
                        self.bindOrdenatorButtons();
                    });

                    clearInterval(interval);
                }
            }, 100);
        }
    }

    static bindStatusButtons() {
        $('a.btn.status').on('click', function(e){
            e.preventDefault();

            $.ajax({
                success: () => $.fn.DataTable.tables().forEach((a) => $(a).DataTable().draw(false)),
                error: () => alert($(this).data('error-message') || 'There was an error processing your request. Please, try again.'),
                processData: false,
                type: 'POST',
                url: $(this).attr('href')
            });
        });
    }

    static bindOrdenatorButtons() {
        $('a.btn.ordenator').on('click', function(e){
            e.preventDefault();

            $.ajax({
                success: () => $.fn.DataTable.tables().forEach((a) => $(a).DataTable().draw(false)),
                error: () => alert($(this).data('error-message') || 'There was an error processing your request. Please, try again.'),
                processData: false,
                type: 'POST',
                url: $(this).attr('href')
            });
        });
    }
}
