import * as $ from 'jquery';
import '../../sass/site/general.scss';
import Header from "./modules/header";
import Origin from "./modules/origin";
import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

global.$ = global.jQuery = $;
global.tlMenuAnimation = gsap.timeline();
global.animationInProgress = false;
global.swiper = null;

$(window).on('load', () => {

    Header.bindHeaderActions();
    Origin.parseOrigin();

    let tlStart = gsap.timeline();
    tlStart.to($('.shine.banner-right'), { duration: 1, opacity: 1, y: 0, ease: Power2.easeOut }, .2)

    if ($('.shine.banner-left').length > 0) {
        tlStart.to($('.shine.banner-left'), { duration: 1, opacity: 1, y: 0, ease: Power2.easeOut }, .4);
    }
});

