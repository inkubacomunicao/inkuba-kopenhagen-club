import Callback from "./modules/callback.js"

$(window).on('load', () => {
    Callback.bindCallbackActions();
});
