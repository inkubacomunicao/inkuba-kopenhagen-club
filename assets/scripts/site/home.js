import Home from "./area/home.js"
import Lgpd from "./modules/lgpd";
import Login from "./modules/login";

$(window).on('load', () => {

    Home.bindHomeActions();
    if (!sessionStorage.getItem("kc-user-complete-lgpd")){
        //MODAL PRIMEIRO ACESSO
        Lgpd.bindLgpdActions();
    } else {
        Login.bindInviteCompleteRegistration();
    }
});
