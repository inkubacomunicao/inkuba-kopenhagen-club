import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

export default class {
    constructor() {
        this.elementID = "";
        this.tlAnimationModal = gsap.timeline();
    }

    bindActions(_id) {
        this.elementID = _id;
        const SELF = this;

        $(`${_id} .btn-close`).click(function(e) {
            let tlCloseButton = gsap.timeline();
            tlCloseButton.to($(e.currentTarget), { duration: 0.3, scale: 0.7, ease: Power2.easeOut })
                .to($(e.currentTarget), { duration: 0.2, scale: 1, ease: Power2.easeOut }, 0.3 );
            SELF.hideModal(_id);
        });

        $('.js-closethis').click(function(e) {
            e.preventDefault();
            SELF.hideModal(_id);
        });

        $('.form-default').click(function(e) {
            e.stopPropagation();
        });
    };

    showModal() {

        if (!global.animationInProgress){

            const ELEMENT_ID = this.elementID;
            $("body").css("overflow", "hidden");
            $(ELEMENT_ID).css({"visibility": "visible", "opacity": 1, "display": "block"});

            global.animationInProgress = true;
            this.tlAnimationModal.kill();
            this.tlAnimationModal = gsap.timeline();

            this.tlAnimationModal.from($(ELEMENT_ID),       { duration: 1.0, opacity: 0, ease: Power2.easeOut })
                .from($(`${ELEMENT_ID} .container-modal`),  { duration: 1.0, y: 50, ease: Power2.easeOut }, 0)
                .from($(`${ELEMENT_ID} .left-panel .text`), { duration: 0.5, x: -20, opacity: 0, ease: Power2.easeOut }, .2)
                .from($(`${ELEMENT_ID} .header`),           { duration: 0.4, x: -10, opacity: 0, ease: Power2.easeOut }, .4)
                .from($(`${ELEMENT_ID} .btn-close`),        { duration: 0.4, x: -10, scale: 0, opacity: 0, ease: Power2.easeOut }, .6)
                .from($(`${ELEMENT_ID} .content`),          { duration: 0.4, x: -10, opacity: 0, ease: Power2.easeOut }, .8)
                .from($(`${ELEMENT_ID} footer`),            { duration: 0.4, x: -10, opacity: 0, ease: Power2.easeOut, onComplete: function(){global.animationInProgress = false}}, 1);
        }

    }

    hideModal() {

        $("body").css("overflow", "auto");
        const ELEMENT_ID = this.elementID;

        this.tlAnimationModal.kill();
        this.tlAnimationModal = gsap.timeline();

        this.tlAnimationModal.to($(`${ELEMENT_ID}`), { duration: 1.0, opacity: 0, ease: Power2.easeOut,
            onComplete:function(){
                $(ELEMENT_ID).css({"visibility": "hidden", "display": "none"});
                window.location.hash = '';
            }
        });
    }

}
