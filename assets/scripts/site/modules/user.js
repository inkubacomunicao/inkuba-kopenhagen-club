import Toastify from "toastify-js";
import Global from "../utils/global";
import Functions from "../utils/functions";
import Modal from "./modal";
import Banner from "./banner";
import Points from "../area/points";
// import ChangePassword from "./change-password";
// import RegisterComplete from "./complete-registration";
import Lgpd from "./lgpd";
import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

export default class {

    static checkToken(_redirect){
        const SELF = this;
        const token = sessionStorage.getItem('kcToken');
        if (token){
            this.welcome(token);
        } else if (_redirect) {
            this.logout();
        } else {
            Banner.bindBannerActions();
            $("#kc-header .navigation .public").css("display", "block");
        }
    }

    static isLogged(){
        const SELF = this;
        const token = sessionStorage.getItem('kcToken');
        return (token);
    }

    static login(_payload, _action, _){
        const SELF = this;
        jQuery.ajax({
            type: "POST",
            url: Global.getURLAPIV2() + 'participant/login',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(_payload),
            success: function( data )
            {
                SELF.welcome(data.jwt);
                SELF.afterLoginActions(_action);
            },
            error: function (request, status, error) {
                $("#btn-submit-login, #btn-submit-login-page").prop("disabled", false).css("cursor", "pointer").removeClass("loading").val("Acessar").removeClass("loading");
                Toastify({
                    text: "Erro ao efetuar o login.",
                    className: "error",
                    close: true,
                    duration: 4000,
                    gravity: "bottom",
                }).showToast();
            }
        });
    }

    static afterLoginActions(_action) {
        switch (_action) {
            case 'login':
            case 'redirect-to-statemant':
                window.location.replace('../');
                break;
            case 'redirect-to-success':
                window.location.replace('../cadastro/sucesso');
                break;
            default:
                break;
        }
    }

    static welcome(_token, _noAnimations) {
        const data = Functions.parseJwt(_token);

        sessionStorage.setItem("kcBirthdate", data.brithdate);
        sessionStorage.setItem("kcExp", data.exp);
        sessionStorage.setItem("kcName", data.name);
        sessionStorage.setItem("kcToken", _token);
        sessionStorage.setItem("kcCompletedRegistry", data.CompletedRegistry);

        this.getStatement('dashboard');
        this.getVouchers();

        // //SE usuário entrou pelo link /#complete-seus-dados
        if (sessionStorage.getItem("user-complete-register")){
            const modalCompleteRegistration = new Modal();
            modalCompleteRegistration.bindActions("#modal-complete-registration");
            RegisterComplete.bindCompleteRegisterActions();
            modalCompleteRegistration.showModal();
            sessionStorage.removeItem("user-complete-register");
        }

        $("html, body").animate({scrollTop : 0},1000);
        $("#kcName").html( Functions.getFirstWord(sessionStorage.getItem('kcName')) );
        $("#kcLgpdName").html( Functions.getFirstWord(sessionStorage.getItem('kcName')) );
        $('.dashboard, .navigation .logged').css({"visibility": "visible", "display": "block"});
        $(".keep-registration, .btn-complete-registration").css("display", "block");
        $("#kc-header, #kc-menu, #kc-footer").addClass("auth");

        setTimeout(()=>{
            $(".box-register, .btn-register").hide();
        },2000);

        if (data.CompletedRegistry==="False" && sessionStorage.getItem("kcCompletedRegistration") !== "true"){
            $(".dashboard .incomplete").show();
            // $(".dashboard .exp").hide();
        }

        if (!_noAnimations){
            this.showDashboard();
            Banner.bindBannerActions();
        }
    }

    static logout( retry ){
        Tween.set('.navigation .public', { opacity: 1 });

        sessionStorage.removeItem("kcBirthdate");
        sessionStorage.removeItem("kcExp");
        sessionStorage.removeItem("kcName");
        sessionStorage.removeItem("kcToken");
        sessionStorage.removeItem("kcDateNextExpiration");
        sessionStorage.removeItem("kcScoreNextExpiration");
        sessionStorage.removeItem("kcAccumulated");
        sessionStorage.removeItem("kcAvailable");
        sessionStorage.removeItem("kcCompletedRegistry");
        sessionStorage.removeItem("kcCompletedRegistration");

        let tlAnimationLogout = gsap.timeline();
        tlAnimationLogout.to('.navigation .logged', { duration: 0.5, opacity: 0, ease: Power2.easeIn })
            .from('.navigation .public', { duration: 1, x: -30, opacity: 0, ease: Power2.easeOut }, 0.5)
            .to('.dashboard', { duration: 1, opacity: 0, ease: Power2.easeOut,
                onComplete: function(){
                    $('.dashboard, .navigation .logged').css({"visibility": "none", "display": "none"});
                    $("#kc-header, #kc-menu, #kc-footer").removeClass("auth");
                }
            }, 0.5);

        window.location.href = "/";
    }

    static getStatement(_action) {
        const SELF = this;
        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPI() + 'participant/statement',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {

                sessionStorage.setItem("kcScoreNextExpiration", (data.value.data.scoreNextExpiration) ? data.value.data.scoreNextExpiration : "0" );
                sessionStorage.setItem("kcDateNextExpiration", (data.value.data.dateNextExpiration) ? Functions.formatDateFull(data.value.data.dateNextExpiration) : "---");
                sessionStorage.setItem("kcAvailable", (data.value.data.available) ? data.value.data.available : "0");

                //SETAR SEXO
                // $("#gender-welcome").html((data.value.data.sex === "F") ? "Bem vinda" : "Bem vindo");

                SELF.afterGetStatmentActions(_action, data.value.data);
            },
            error: function (request, status, error) {
                Toastify({
                    text: "Sua sessão expirou. Faça o login para continuar navegando.",
                    className: "error",
                    close: true,
                    duration: 5000,
                    gravity: "bottom",
                    callback: function() {
                        SELF.logout();
                    }
                }).showToast();
                console.log((request.responseJSON.value || request.responseJSON.error).message);
            }
        });
    }

    static getVouchers() {
        const SELF = this;
        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPI() + 'participant/vouchers',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {
                //console.log("GET VOUCHERS", data);
                $("#kcVouchersDisponiveis").html(data.value.data.voucherCount);
                // (data.value.data.listaVoucher.length) ? $("#kcVouchersDisponiveis").html(data.value.data.listaVoucher.length) : $("#kcVouchersDisponiveis").html("0");
            }
        });
    }

    static afterGetStatmentActions(_action, _data){
        switch (_action) {
            case 'dashboard':
                $("#kcScoreNextExpiration").html(sessionStorage.getItem('kcScoreNextExpiration'));
                $("#kcDateNextExpiration").html(sessionStorage.getItem('kcDateNextExpiration'));
                $("#kcAvailable").html(sessionStorage.getItem('kcAvailable'));
                break;

            case 'points':
                $("#kcScoreNextExpiration").html(sessionStorage.getItem('kcScoreNextExpiration'));
                $("#kcDateNextExpiration").html(sessionStorage.getItem('kcDateNextExpiration'));
                $("#kcAccumulated").html(sessionStorage.getItem('kcAccumulated'));
                break;

            default:
                break;
        }
    }

    static showDashboard() {

        let tlAnimationWelcome = gsap.timeline();
        tlAnimationWelcome.to('.navigation .public',    { duration: 1.5, opacity: 1, ease: Power2.easeOut         }, 1.0)
            .to('.navigation .logged',                  { duration: 1.0, x: 0, opacity: 1, ease: Power2.easeOut   }, 1.5)
            .to('.dashboard',                           { duration: 1.5, opacity: 1, ease: Power2.easeOut         }, 1.4)
            .from('.dashboard .curr',                   { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 1.8)
            .from('.resume-dashboard .number',          { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.0)
            .from('.resume-dashboard .text',            { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.1)
            .from('.dashboard .next',                   { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.2)
            .from('.dashboard .exp',                    { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.3)
            .from('.dashboard .vouchers',               { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.3)
            .from('.dashboard .voucher-exp',            { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.35)
            .from('.dashboard .incomplete',             { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.4)
            .from('.dashboard footer',                  { duration: 0.8, x: -20, opacity: 1, ease: Power2.easeOut }, 2.5);
    }

    static updateData(_location) {
        if (_location === "meus-pontos") {
            Points.getData();
        } else {
            this.welcome(sessionStorage.kcToken, true);
        }
    }
}
