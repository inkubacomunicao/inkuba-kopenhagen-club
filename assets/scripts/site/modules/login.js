import Functions from "../utils/functions";
import User from "./user";
import Modal from "./modal";
import Lgpd from "./lgpd";
import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

export default class {
    constructor() {
        this.tlAnimationTooltip = gsap.timeline();
        this.isOpen = false;
        this.animationInProgress = false;
    }

    bindLoginActions() {

    }

    static bindInviteCompleteRegistration() {

        User.checkToken();

        //MODAL COMPLETAR CADASTRO
        const dataToken = Functions.parseJwt(sessionStorage.getItem("kcToken"));
        if (dataToken && dataToken.CompletedRegistry==="False"){
            const modalInviteCompleteRegistration = new Modal();
            modalInviteCompleteRegistration.bindActions("#modal-invite-complete-registration");
            modalInviteCompleteRegistration.showModal();
        }
    }

}
