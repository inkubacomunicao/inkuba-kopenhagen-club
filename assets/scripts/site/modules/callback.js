import User from "./user";

export default class {
    static bindCallbackActions() {
        const SELF = this;
        const qs = new URLSearchParams(window.location.search);

        if (qs.has('state')) {
            switch (qs.get('state')) {
                case 'login':
                    User.login({ code: qs.get('code') }, 'login');
                    break;
                case 'password':
                case 'update':
                    // Se necessária confirmação de cadastro, o evento deve ser disparado aqui.
                    // Sem o break, o update leva para o caso default redirecionando para a home.
                default:
                    window.location.href = '/';
                    break;
            }
        } else {
            window.location.href = '/';
        }
    }
}
