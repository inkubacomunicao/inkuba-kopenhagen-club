import Swiper from "swiper";
import Global from "../utils/global";
import User from "../modules/user";
import Functions from "../utils/functions";
import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

export default class {

    static bindBannerActions(){

        if (User.isLogged()){
            this.getDataAuthenticated();
        } else {
            this.getDataAnonymous();
        }

    }

    static getDataAnonymous() {

        const SELF = this;
        jQuery.ajax({
            type: "GET",
            url: Global.getURLAPI() + 'banner/contextual',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {
                SELF.insertDataBanner(data);
            },
            error: function (request, status, error) {
                console.log( "Erro ao carregar os banners: " + (request.responseJSON.value || request.responseJSON.error).message);
            }
        });

    }

    static getDataAuthenticated() {

        const SELF = this;
        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPI() + 'banner/contextual',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {
                SELF.insertDataBanner(data);
            },
            error: function (request, status, error) {
                console.log( "Erro ao carregar os banners: " + (request.responseJSON.value || request.responseJSON.error).message);
            }
        });

    }

    static insertDataBanner(_data) {

        gsap.to("#banner-content", .3, { opacity: 0, ease: Power2.easeOut });
        $(".swiper-pagination").remove();

        let content = "";
        $.each(_data, function( i, value ) {

          if (_data[i].title) {
              content += '<div class="swiper-slide">';
              content += '<div class="' + ((!_data[i].title) ? 'banner no-mask' : 'banner') + '">';
              if (_data[i].title) {
                  content += '  <div class="ct-caption">';
                  content += '    <div class="line"></div>' +
                      '           <div class="caption">' +
                      //'             <h4>Programa de vantagens</h4>' +
                      '               <h2>' + _data[i].title + '</h2>' +
                      '               <p>' + ((_data[i].description) ? _data[i].description : '') + '</p>' +
                      '               <a ' + ((_data[i].url) ? 'href="' + _data[i].url + '"' : '') + ' class="btn-default border type2" target="' + ((_data[i].url) ? ( _data[i].url.indexOf( 'kopclub' ) >= 0 ? '_self' : '_blank' ) : '') + '">Entrar para o Kop Club</a>' +
                      '           </div>' +
                      '       </div>';
              }
              content += '<div class="img-desktop" style="background-image: url(/uploads/images/' + _data[i].image + ');"></div>';
              content += '<div class="img-mobile"><img src="/uploads/images/' + _data[i].imageMobile + '" alt="' + _data[i].title + '" /></div>';
              content += '</div>';
              content += '</div>';
          } else {
              content += '<div class="swiper-slide">';
              content += '<a ' + ((_data[i].url) ? 'href="' + _data[i].url + '"' : '') + ' target="' + ((_data[i].url) ? ( _data[i].url.indexOf( 'kopclub' ) >= 0 ? '_self' : '_blank' ) : '') + '">';
              content += '<div class="' + ((!_data[i].title) ? 'banner no-mask' : 'banner') + '">';
              content += '<div class="img-desktop" style="background-image: url(/uploads/images/' + _data[i].image + ');"></div>';
              content += '<div class="img-mobile"><img src="/uploads/images/' + _data[i].imageMobile + '" alt="' + _data[i].title + '" /></div>';
              content += '</div>';
              content += '</a>';
              content += '</div>';

          }
        });

        const pagination = ((_data.length>1) ? '<div class="swiper-pagination"></div>' : "");

        const ctBanner = $("#banner-content");
        ctBanner.html(content);
        ctBanner.after(pagination);

        this.bindSwiperActions();

    }

    static bindSwiperActions(){

        var _this = this;

        if (global.swiper){
            console.log('Destroy', global.swiper)
            global.swiper.destroy();
        }

        global.swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            loop: true,
            autoplay: 5000,
            speed: 1000
        });

        gsap.to("#banner-content", { duration: 1.5, opacity: 1, ease: Power2.easeOut, delay: 0.3 });

        $(window)
          .unbind( 'hashchange', Functions.checkHash )
          .bind( 'hashchange', Functions.checkHash );

        switch (window.location.hash) {
            case "#login":
                $("#btn-login").trigger('click');
                break;

            default:
                break;
        }

        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName( 'script' )[ 0 ];
        firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );

        $( '.swiper-slide' )
          .find( 'a' )
            .on( 'click', function() {

              var ref = $( this ).attr( 'href' );

              if( ref.indexOf( 'video' ) > 0 ) {

                var hashVideo = ref.slice( ref.indexOf( 'video' ) + 6 );

                _this.videoOpen( hashVideo );

              }

            } );

        $( '.overlay-close' )
          .on('click', _this.videoClose );
    }

    static videoOpen( hashVideo ) {

      var player = new YT.Player( 'video', {
          width: '100%',
          height: '100%',
          frameborder: '0',
          videoId: hashVideo,
          playerVars: {
              'rel': 0,
              'controls': 0,
              'autoplay': 1,
              'wmode': 'transparent',
              'autohide': 1,
              'showinfo': 0
          },
          events: {
              'onStateChange': function() {}
          }
      });

      setTimeout(function() {

        $( '.video-overlay' )
            .fadeIn( 250 );

      }, 1000);

    }

    static videoClose() {

      $('.video-overlay')
          .fadeOut( 250 );

      setTimeout(function() {

        $( '.video-home' )
            .remove();

        $( '.video-overlay' )
            .append( '<div id="video" class="video-home"></div>' )

      }, 250);

    }
}
