export default class {
    static parseOrigin() {
        const qs = new URLSearchParams(window.location.search);
        const origin = `${qs.get('utm_source') || ''}/${qs.get('utm_medium') || ''}/${qs.get('utm_campaign') || ''}/${qs.get('utm_content') || ''}`.replace(/^\//gi, '').replace(/\/$/gi, '').replace(/\/{2,}/gi, '').trim();

        origin != null && origin != undefined && origin.replace("/", "").trim().length > 0 && sessionStorage.setItem("origin", origin);
    }

    static getOrigin() {
        const origin = sessionStorage.getItem("origin");

        return origin != null && origin != undefined && origin.replace("/", "").trim().length > 0 ? origin : null;
    }

    static clearOrigin() {
        sessionStorage.removeItem("origin");
    }
}
