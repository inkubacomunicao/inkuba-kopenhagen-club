import Toastify from 'toastify-js';
import Global from '../utils/global';
import Origin from '../modules/origin';
import Functions from '../utils/functions';
import User from './user';
import Modal from "./modal";
import Login from "./login";

export default class {

    static getData() {

        const SELF = this;

        if (sessionStorage.getItem("kcToken")){
            jQuery.ajax({
                type: "GET",
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
                },
                url: Global.getURLAPIV2() + 'participant/me',
                dataType: 'json',
                contentType: 'application/json',
                success: function( data )
                {
                    const userData = data;

                    if (!userData.receive_Email && !userData.receive_Sms && !userData.receive_AppMSG) {
                        window.userData = userData;
                        window.dtRegister = userData.dtRegister;

                        $('input[name=receive_Email_lgpd]').prop('checked', (userData.receive_Email));
                        $('input[name=receive_Sms_lgpd]').prop('checked', (userData.receive_Sms));
                        $('input[name=receive_AppMSG_lgpd]').prop('checked', (userData.receive_AppMSG));

                        const modalLgpd = new Modal();
                        modalLgpd.bindActions("#modal-lgpd");
                        modalLgpd.showModal();

                        SELF.bindFormLgpdAction();
                    }

                },
                error: function (request, status, error) {
                    Toastify({
                        text: "Sua sessão expirou. Faça o login para continuar navegando.",
                        className: "error",
                        close: true,
                        duration: 5000,
                        gravity: "bottom",
                        callback: function() {
                            User.logout( true );
                        }
                    }).showToast();
                }
            });
        }
    }

    static bindLgpdActions() {

        const SELF = this;

        this.getData();
    }

    static bindFormLgpdAction(){

        $("#form-lgpd")[0].reset();

        $("#modal-lgpd .btn-close").click(function(){
            Login.bindInviteCompleteRegistration();
        });

        $("#btn-no-lgpd").click((e)=>{
            e.preventDefault();
            sessionStorage.setItem("kc-user-complete-lgpd", "true");
            $("#modal-lgpd .btn-close").click();
        });

        $('input[name=receive_all]').click((e)=>{
            //e.preventDefault();
            if ($('input[name=receive_all]').is(':checked')){
                $('input[name=receive_Email_lgpd]').prop('checked', true);
                $('input[name=receive_Sms_lgpd]').prop('checked', true);
                $('input[name=receive_AppMSG_lgpd]').prop('checked', true);
            } else {
                $('input[name=receive_Email_lgpd]').prop('checked', false);
                $('input[name=receive_Sms_lgpd]').prop('checked', false);
                $('input[name=receive_AppMSG_lgpd]').prop('checked', false);
            }
        });

        const formLgpd = $( "#form-lgpd" );
        const btnSubmit = $("#btn-submit-form-lgpd");

        formLgpd.unbind().submit(function(e){

            e.preventDefault();

            btnSubmit.prop("disabled", true).css("cursor", "wait").val("Enviando...");
            const dados = Functions.getFormData($(this));
            const updatedFrom = Origin.getOrigin();

            const payload = {
                receive_Email: (dados.receive_Email_lgpd==="on"),
                receive_Sms: (dados.receive_Sms_lgpd==="on"),
                receive_AppMSG: (dados.receive_AppMSG_lgpd==="on"),
            };

            jQuery.ajax({
                type: "PATCH",
                headers: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
                },
                url: Global.getURLAPIV2() + 'participant/me',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(payload),
                success: function( data )
                {
                    btnSubmit.prop("disabled", false).css("cursor", "pointer").val("Salvar");

                    gtag('event', 'view', {'event_category': 'Completar Modal LGPD', 'event_label': 'Sucesso_' + window.dtRegister });

                    sessionStorage.setItem("kc-user-complete-lgpd", "true");

                    Toastify({
                        text: "Preferências atualizadas com sucesso!",
                        className: "success",
                        close: true,
                        duration: 3000,
                        gravity: "bottom",
                    }).showToast();

                    $("#modal-lgpd .btn-close").click();
                },
                error: function (request, status, error) {
                    $("html, body, .overlay").animate({scrollTop : 0},1000);
                    btnSubmit.prop("disabled", false).css("cursor", "pointer").val("Salvar").removeClass("loading");
                    Toastify({
                        text: "Erro ao atualizar preferências: " + (request.responseJSON.value || request.responseJSON.error).message,
                        className: "error",
                        close: true,
                        duration: 4000,
                        gravity: "bottom",
                    }).showToast();
                }
            });

            return false;

        });
    }
}
