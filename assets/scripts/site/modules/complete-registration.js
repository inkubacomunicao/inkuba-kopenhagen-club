import Toastify from 'toastify-js';
import Global from '../utils/global';
import User from './user';

export default class {

    static bindCompleteRegisterActions() {
        const SELF = this;

        this.getData();
    }

    static getData() {
        const SELF = this;

        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPIV2() + 'participant/update',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data ) {
                window.location.href = data.url;
            },
            error: function (request, status, error) {
                Toastify({
                    text: "Sua sessão expirou. Faça o login para continuar navegando.",
                    className: "error",
                    close: true,
                    duration: 5000,
                    gravity: "bottom",
                    callback: function() {
                        User.logout( true );
                    }
                }).showToast();
            }
        });
    }
}
