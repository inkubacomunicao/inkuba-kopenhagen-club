import Modal from './modal';
// import Login from './login';
// import Register from './register';
import RegisterComplete from './complete-registration';
// import ForgotPassword from './forgot-password';
import User from './user';
import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

export default class {

    static bindHeaderActions() {

        const SELF = this;

        User.checkToken();

        // Register.bindRegisterActions( $("input[name=cpf_login]").val() );

        // //MODAL CADASTRO COMPLETO
        // const modalCompleteRegistration = new Modal();
        // modalCompleteRegistration.bindActions("#modal-complete-registration");

        // //MODAL ESQUECI A SENHA
        // const modalForgotPassword = new Modal();
        // modalForgotPassword.bindActions("#modal-forgot-password");

        // //MODAL PRIMEIRO ACESSO
        // const modalFirstAccess = new Modal();
        // modalFirstAccess.bindActions("#modal-first-access");

        // const login = new Login();
        // login.bindLoginActions();

        //Menu
        $("#btn-menu").click(function (e) {
            e.preventDefault();
            SELF.toggleMenu();
        });

        //Menu
        $("#btn-close-menu").click(function (e) {
            e.preventDefault();
            SELF.toggleMenu();
        });

        // //Primeiro acesso
        // $(".btn-first-access").click(function (e) {
        //     e.preventDefault();
        //     login.hideTooltip();
        //     modalFirstAccess.showModal();
        // });

        //Cadastro completo
        $("#btn-complete-registration, .btn-complete-registration, .btn-my-profile").click(function (e) {
            e.preventDefault();
            RegisterComplete.bindCompleteRegisterActions();
        });

        // //Cadastro completo
        // $(".btn-my-profile").click(function (e) {
        //     e.preventDefault();
        //     RegisterComplete.bindCompleteRegisterActions();
        //     SELF.closeMenu();
        //     modalCompleteRegistration.showModal();
        // });

        // //Esqueci a senha
        // $(".btn-forgot-password").click(function (e) {
        //     e.preventDefault();
        //     ForgotPassword.bindForgotPasswordActions(($("input[name=cpf_login]").val()) ? $("input[name=cpf_login]").val() : $("input[name=cpf_login_page]").val());
        //     login.hideTooltip();
        //     modalForgotPassword.showModal();
        // });

        // //Alterar senha
        // $(".btn-change-password").click((e)=>{
        //     e.preventDefault();
        //     // SELF.closeMenu();
        //     User.changePassword();
        // });

        //Logout
        $(".btn-logout").click(function (e) {
            e.preventDefault();
            User.logout();
        });

    }

    static toggleMenu() {
        const isOpen = $("#kc-menu").hasClass("isOpen");
        if(isOpen){
            this.closeMenu();
        } else {
            this.showMenu();
        }
    }

    static showMenu() {

        const menu = $("#kc-menu");
        const bgMenu = $("#kc-header .bg-open");
        const btnCloseMenu = $("#kc-header .btn-close");
        menu.css( {"visibility": "visible", "display": "block" });
        bgMenu.css("visibility", "visible");
        btnCloseMenu.css("visibility", "visible");
        $("#btn-menu").addClass("open");

        global.tlMenuAnimation.kill();
        global.tlMenuAnimation = gsap.timeline();
        global.tlMenuAnimation.from(menu, { duration: 1, opacity: 0, ease: Power2.easeOut })
            .from(bgMenu, { duration: 1, opacity: 0, ease: Power2.easeOut }, 0)
            .to($('.navigation .public, .navigation .logged'), { duration: 1, opacity: 0, ease: Power2.easeOut, onComplete: function(){
                    $('.navigation .public, .navigation .logged').css("visibility", "hidden");
                }
            }, 0.4)
            .from('.ct-menu ul li',                     { duration: 1.5, opacity: 0, x: -30, ease: Power2.easeOut, stagger: 0.3 }, 0.4)
            .from('.social ul li',                      { duration: 1.5, opacity: 0, x: -30, ease: Power2.easeOut, stagger: 0.2 }, 0.3)
            .from(btnCloseMenu,                         { duration: 1.5, opacity: 0, x: -30, ease: Power2.easeOut }, .6)
            .from($('#kc-header .btn-close .line1'),    { duration: 1.0, rotation: 0, ease: Power2.easeOut }, 1.2)
            .from($('#kc-header .btn-close .line2'),    { duration: 1.0, rotation: 0, ease: Power2.easeOut }, 1.2)
            .from($(".social h3"),                      { duration: 1.5, x: -30, opacity: 0, ease: Power2.easeOut }, .7);

        menu.addClass("isOpen");
        $("#kc-header").addClass("isOpen");
    }

    static closeMenu() {

        const menu = $("#kc-menu");
        const bgMenu = $("#kc-header .bg-open");
        const btnCloseMenu = $("#kc-header .btn-close");

        if ($("#kc-header").hasClass("auth")){
            $('.navigation .logged').css("visibility", "visible");
        } else {
            $('.navigation .public').css("visibility", "visible");
        }

        $("#btn-menu").removeClass("open");

        global.tlMenuAnimation.kill();
        global.tlMenuAnimation = gsap.timeline();
        global.tlMenuAnimation
            .to(menu, { duration: 1, opacity: 0, ease: Power2.easeOut, onComplete: function(){
                    menu.css( {"visibility": "hidden", "display": "none" });
                    bgMenu.css("visibility", "hidden");
                    btnCloseMenu.css("visibility", "hidden");
                    Tween.set(menu, { opacity: 1 });
                    Tween.set(bgMenu, { opacity: 1 });
                    Tween.set('.ct-menu ul li', { opacity: 1, delay: 0.5 });
                    Tween.set('.social ul li', { opacity: 1, delay: 0.5 });
                    Tween.set(btnCloseMenu, { opacity: 1, delay: 0.5 });
                    Tween.set('#kc-header .btn-close .line1', { rotation: 50 });
                    Tween.set('#kc-header .btn-close .line2', { rotation: -50 });
                    Tween.set('.social h3', { opacity: 1 });
                }
            }, 0.5)
            .to(bgMenu, { duration: 1, opacity: 0, ease: Power2.easeOut }, 0.5)
            .to($('.navigation .public, .navigation .logged'), { duration: 1, opacity: 1, ease: Power2.easeOut}, 0.4)
            .to('.ct-menu ul li',               { duration: .5, opacity: 0, ease: Power2.easeOut, stagger: 0.1 }, 0)
            .to('.social ul li',                { duration: .5, opacity: 0, ease: Power2.easeOut, stagger: 0.2 }, 0.15)
            .to(btnCloseMenu,                   { duration: .5, opacity: 0, ease: Power2.easeOut}, 0.4)
            .to('#kc-header .btn-close .line1', { duration: .5, rotation: 0, ease: Power2.easeOut}, 0)
            .to('#kc-header .btn-close .line2', { duration: .5, rotation: 0, ease: Power2.easeOut}, 0)
            .to('.social h3',                   { duration: .5, opacity: 0, ease: Power2.easeOut}, 0);

        menu.removeClass("isOpen");
        $("#kc-header").removeClass("isOpen");
    }

}
