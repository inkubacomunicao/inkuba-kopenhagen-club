import validate from 'jquery-validation';
import mask from 'jquery-mask-plugin';
import Toastify from 'toastify-js';
import Global from '../utils/global';
import User from '../modules/user';
import Origin from '../modules/origin';
import Functions from '../utils/functions';

export default class {

    static bindRegisterActions(_cpf) {
        const SELF = this;

        const behavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            options = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(behavior.apply({}, arguments), options);
                }
            };
        $('input[name=telephone_register]').mask(behavior, options);

        $('input[name=cpf_register]').val(_cpf).mask('000.000.000-00');
        $('input[name=birthDate_register]').mask('00/00/0000');

        $.validator.addMethod(
            "brazilianDate",
            function(value, element) {
                // put your own logic here, this is just a (crappy) example
                return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
            },
            "* Favor digitar a data no formato DD/MM/AAAA."
        );

        $.validator.addMethod("customemail",
            function(value, element) {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            },
            "E-mail inválido."
        );

        this.bindValidateAction();

        this.bindSubmitAction();

    }

    static bindValidateAction() {

        const formRegistration = $( "#form-registration" );
        const btnSubmit = $("#btn-submit-register");
        formRegistration.validate( {
            errorClass: "error-border",
            rules: {
                name_register: "required",
                cpf_register: "required",
                birthDate_register: {
                    required: true,
                    brazilianDate: true
                },
                email_register: {
                    required: true,
                    customemail: true
                },
                password_register: {
                    required: true,
                    minlength: 6
                },
                passwordconfirm_register: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password_register"
                },
                telephone_register: {
                    required: true,
                    minlength: 15
                },
                agreedTermsConditions_register: "required"

            },
            messages: {
                name_register: "* Favor preencher nome",
                cpf_register: "*  Favor preencher CPF",
                birthDate_register: {
                    required: "* Favor preencher data nascimento",
                },
                email_register: {
                    required: "* Favor preencher e-mail",
                    email: "* E-mail inválido"
                },
                password_register: {
                    required: "* Favor criar senha",
                    minlength: "* Mínimo 6 caracteres"
                },
                passwordconfirm_register: {
                    required: "* Favor confirmar senha",
                    minlength: "* Mínimo 6 caracteres",
                    equalTo: "* Senhas não são iguais"
                },
                telephone_register: {
                    required: "* Favor preencher celular",
                    minlength: "* Celular inválido"
                },
                agreedTermsConditions_register: "* Para se cadastrar é necessário aceitar o Regulamento."
            },
            errorPlacement: function(error, element) {
                $(element).parent().find(".error").html(error);
            },
            debug: false
        } );

    }

    static bindSubmitAction() {

        const formRegistration = $( "#form-registration" );
        const btnSubmit = $("#btn-submit-register");
        formRegistration.unbind().submit(function(e){
            e.preventDefault();

            if (formRegistration.valid()){

                btnSubmit.prop("disabled", true).css("cursor", "wait").val("Enviando...").addClass("loading");
                const dados = Functions.getFormData($(this));
                const includedFrom = Origin.getOrigin();

                const payload = {
                    name: dados.name_register,
                    cpf: Functions.numberFy(dados.cpf_register),
                    birthDate: Functions.formatDate(dados.birthDate_register),
                    email: dados.email_register,
                    password: dados.password_register,
                    telephone: parseInt(Functions.numberFy(dados.telephone_register)),
                    receive_Email: (dados.receive_Email_register==="on"),
                    receive_Sms: (dados.receive_Sms_register==="on"),
                    receive_AppMSG: (dados.receive_AppMSG_register==="on"),
                    agreedTermsConditions: (dados.agreedTermsConditions_register==="on"),
                    includedFrom
                };

                jQuery.ajax({
                    type: "POST",
                    url: Global.getURLAPI() + 'participant',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(payload),
                    success: function( data )
                    {
                        gtag('event', 'view', {'event_category': 'Cadastro' ,'event_label': 'Sucesso' });

                        Toastify({
                            text: "Cadastro efetuado com sucesso! O Login será efetuado automaticamente...",
                            close: true,
                            className: "success",
                            duration: 4000,
                            gravity: "bottom",
                            callback: function() {
                                btnSubmit.prop("disabled", false).css("cursor", "pointer").val("Fazer Parte!").removeClass("loading");
                            }
                        }).showToast();

                        Origin.clearOrigin();

                        User.login({cpf: payload.cpf, password: payload.password}, 'redirect-to-success');
                    },
                    error: function (request, status, error) {
                        btnSubmit.prop("disabled", false).css("cursor", "pointer").val("Fazer Parte!").removeClass("loading");
                        Toastify({
                            text: "Erro ao efetuar o cadastro: " + (request.responseJSON.value || request.responseJSON.error).message,
                            className: "error",
                            close: true,
                            duration: 4000,
                            gravity: "bottom",
                        }).showToast();
                    }
                });

                return false;
            }

        });

    }
}
