import validate from 'jquery-validation';
import mask from 'jquery-mask-plugin';
import Toastify from 'toastify-js';
import Global from '../utils/global';
import Functions from '../utils/functions';
import Modal from "../modules/modal";
import User from "../modules/user.js";

export default class {

    static bindContactActions() {

        const behavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            options = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(behavior.apply({}, arguments), options);
                }
            };
        $('input[name=celular]').mask(behavior, options);

        if (sessionStorage.getItem("kcToken")){
            this.getUserData();
        }
        this.bindValidateAction();
        this.bindSubmitAction();

    }

    static bindValidateAction() {

        const formContact = $( "#form-contact" );
        const btnSubmit = $("#btn-submit-contact");
        formContact.validate( {
            errorClass: "error-border",
            rules: {
                name: "required",
                surname: "required",
                email: {
                    required: true,
                    email: true
                },
                cpf: "required",
                subject: "required",
                description: "required",

            },
            messages: {
                name: "* Favor preencher o nome",
                surname: "*  Favor preencher o sobrenome",
                email: {
                    required: "* Favor preencher e-mail",
                    email: "* E-mail inválido"
                },
                cpf: "* Favor preencher o CPF",
                subject: "* Favor preencher o assunto",
                description: "*  Favor preencher a mensagem",
            },
            errorPlacement: function(error, element) {
                $(element).parent().find(".error").html(error);
            },
            debug: false
        } );
    }

    static bindSubmitAction() {

        const formContact = $( "#form-contact" );
        const btnSubmit = $("#btn-submit-contact");
        formContact.unbind().submit(function(e){
            e.preventDefault();

            if (formContact.valid()){

                btnSubmit.prop("disabled", true).css("cursor", "wait").val("Enviando...").addClass("loading");
                const dados = Functions.getFormData($(this));

                const payload = {
                    name: dados.name,
                    surname: dados.surname,
                    email: dados.email,
                    subject: dados.subject,
                    description: dados.description,
                    cpf: dados.cpf,
                    celular: dados.celular,
                };

                jQuery.ajax({
                    type: "POST",
                    url: Global.getURLAPI() + 'contact',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(payload),
                    success: function( data )
                    {
                        btnSubmit.prop("disabled", false).css("cursor", "pointer").val("Enviar").removeClass("loading");
                        formContact[0].reset();

                        //MODAL SUCCESS
                        const modalContactSuccess = new Modal();
                        modalContactSuccess.bindActions("#modal-contact-success");
                        modalContactSuccess.showModal();

                    },
                    error: function (request, status, error) {
                        Toastify({
                            text: "Erro ao enviar a mensagem, tente novamente.",
                            className: "error",
                            close: true,
                            duration: 3000,
                            gravity: "bottom",
                            callback: function() {
                                btnSubmit.prop("disabled", false).css("cursor", "pointer").val("Enviar").removeClass("loading");;
                            }
                        }).showToast();
                    }
                });

                return false;
            }

        });
    }

    static getUserData() {

        $("input[name=name]").addClass("loading");
        $("input[name=surname]").addClass("loading");
        $("input[name=email]").addClass("loading");
        $("input[name=cpf]").addClass("loading");
        $("input[name=celular]").addClass("loading");

        const SELF = this;
        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPIV2() + 'participant/me',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {
                const userData = data;

                $("input[name=name]").val(Functions.getFirstWord(userData.name)).removeClass("loading");
                $("input[name=surname]").val(Functions.getLastWord(userData.name)).removeClass("loading");
                $("input[name=email]").val(userData.email).removeClass("loading");
                $("input[name=cpf]").val(userData.cpf).removeClass("loading");
                $("input[name=celular]").val(userData.telephone).removeClass("loading");
                $("input[name=subject]").focus();

            },
            error: function (request, status, error) {
                Toastify({
                    text: "Sua sessão expirou. Faça o login para continuar navegando.",
                    className: "error",
                    close: true,
                    duration: 5000,
                    gravity: "bottom",
                    callback: function() {
                        User.logout();
                    }
                }).showToast();
            }
        });

    }
};
