import Global from "../utils/global";
import User from "../modules/user";
import Functions from "../utils/functions";
import Toastify from "toastify-js";
import Modal from "../modules/modal";
import Draggable from "gsap/Draggable";

global.dataAccount = [];

export default class {
    static bindPointsActions() {
        this.getData();
    }

    static getData() {

        const SELF = this;
        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPI() + 'participant/statement',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {
                $("#kcScoreNextExpiration").html((data.value.data.scoreNextExpiration) ? data.value.data.scoreNextExpiration : "0");
                $("#kcDateNextExpiration").html((data.value.data.dateNextExpiration) ? Functions.formatDateFull(data.value.data.dateNextExpiration) : "---");
                $("#kcAvailable").html((data.value.data.available) ? data.value.data.available : "0");

                if (data.value.data.cupomAccount){
                    SELF.insertDataTable(data.value.data.cupomAccount);
                } else {
                    $(".txt-carregando").html("Nenhuma movimentação registrada.");
                }

                global.dataAccount = data.value.data.cupomAccount;
            },
            error: function (request, status, error) {
                Toastify({
                    text: "Sua sessão expirou. Faça o login para continuar navegando.",
                    className: "error",
                    close: true,
                    duration: 3000,
                    gravity: "bottom",
                    callback: function() {
                        User.logout();
                    }
                }).showToast();
                console.log((request.responseJSON.value || request.responseJSON.error).message);
            }
        });
    }

    static insertDataTable(_data){

        const SELF = this;

        let content = "";
        $.each(_data, (i)=>{
            content+='<tr>';
            content+= '<td class="ico">';
            if (_data[i].itens.length){
                content+= '<a class="ico-cupom btn-coupon" data-index-cupom="' + i + '" href="#">Clique aqui</a>';
            }
            content+= '</td>';
            content+='<td>' + [(_data[i].data) ? Functions.formatDateFull(_data[i].data) : "---"] + '</td>';
            content+='<td>' + [(_data[i].storeName) ? _data[i].storeName : "---"] + '</td>';
            content+='<td>' + [(_data[i].description) ? _data[i].description : "---"]+ '</td>';
            content+='<td>' + [(_data[i].value) ? Functions.toReal(_data[i].value) : "---"] + '</td>';
            content+='<td>' + [(_data[i].points) ? _data[i].points : "---"] + '</td>';
            content+='<td>' + [(_data[i].expirationDate) ? Functions.formatDateFull(_data[i].expirationDate) : "---"] + '</td>';
            content+='<td><strong>' + [(_data[i].balance) ? _data[i].balance : "---"] + '<strong></td></tr>';
        });

        $(".txt-carregando").remove();
        $("#data-points").html(content);

        $(".btn-coupon").unbind().click(function(e){
            e.preventDefault();
            SELF.detailCoupon($(this).attr("data-index-cupom"));
        });

    }

    static detailCoupon(_index){
        const modalCoupon = new Modal();
        modalCoupon.bindActions("#modal-coupon");
        modalCoupon.showModal();
        // if ($(window).width()>768){
        //     Draggable.create("#modal-coupon .container-modal", {
        //         type:"x,y",
        //         dragClickables: false,
        //         edgeResistance: 0.65,
        //         bounds: $("#modal-coupon .overlay"),
        //         throwProps: true,
        //         cursor: "auto"
        //     });
        // }

        const DATA = global.dataAccount[_index].itens;

        $("#store-name").html(global.dataAccount[_index].storeName);

        let content = "";
        $.each(DATA, (i)=>{
            content+='<div class="product">';
            content+= '<div class="name">' + DATA[i].productName + '</div>';
            content+= '<div class="amount">' + DATA[i].quantity + '</div>';
            content+= '<div class="unit">' + Functions.toReal(DATA[i].value) + '</div>';
            content+= '<div class="total">' + Functions.toReal(DATA[i].totalValue) + '</div>';
            content+='</div>';
        });

        $("#content-coupon").html(content);
    }

};
