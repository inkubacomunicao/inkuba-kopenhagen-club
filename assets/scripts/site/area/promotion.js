import Swiper from 'swiper';
import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";
export default class {
    static bindPromotionActions() {
        // let tlStart = gsap.timeline();
        // tlStart.to($('body'), 1, { opacity: 1, ease: Power2.easeInOut });

        let swiper = new Swiper('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            renderBullet: function (index, className) {
                return '<span class="' + className + '"></span>';
            },
            keyboard: {
                enabled: true,
            },
            autoplay: {
                delay: 7000,
                disableOnInteraction: false,
            },
            fadeEffect: {
                crossFade: true
            },
        });

        gsap.to(".swiper-wrapper", { duration: 1.5, opacity: 1, ease: Power2.easeOut, delay: 0.3 });
    }
};
