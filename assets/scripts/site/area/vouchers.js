import Global from "../utils/global";
import User from "../modules/user";
import Functions from "../utils/functions";
import Toastify from "toastify-js";
import Modal from "../modules/modal";
import Draggable from "gsap/Draggable";

global.dataVouchers = [];

export default class {
    static bindPointsActions() {
        this.getData();
    }

    static getData() {

        const SELF = this;
        jQuery.ajax({
            type: "GET",
            headers: {
                'Authorization': 'Bearer ' + sessionStorage.getItem("kcToken")
            },
            url: Global.getURLAPI() + 'participant/vouchers',
            dataType: 'json',
            contentType: 'application/json',
            success: function( data )
            {
                //console.log("VOUCHERS", data);

                $("#kcVouchersAvailable").html((data.value.data.listaVoucher) ? data.value.data.listaVoucher.length : "0");
                $("#kcVouchersDateNextExpiration").html((data.value.data.nextExpirationVoucher) ? Functions.formatDateFull(data.value.data.nextExpirationVoucher) : "---");
                if (!sessionStorage.getItem("kcNextVoucherExpirationDate")) {
                    $("#kcVouchersDateNextExpiration").css("fontFamily", "Omnes");
                }

                if (data.value.data.listaVoucher){
                    SELF.insertDataTable(data.value.data.listaVoucher);
                } else {
                    $(".txt-carregando").html("Nenhum voucher registrado.");
                }

                global.dataVouchers = data.value.data.listaVoucher;
            },
            error: function (request, status, error) {
                Toastify({
                    text: "Sua sessão expirou. Faça o login para continuar navegando.",
                    className: "error",
                    close: true,
                    duration: 3000,
                    gravity: "bottom",
                    callback: function() {
                        User.logout();
                    }
                }).showToast();
                console.log((request.responseJSON.value || request.responseJSON.error).message);
            }
        });
    }

    static insertDataTable(_data){

        const SELF = this;

        let content = "";
        $.each(_data, (i)=>{
            content+='<tr>';
            if (!_data[i].rescued && !_data[i].expired) {
                content += '<td class="ico"><a class="ico-voucher btn-voucher" data-index-voucher="' + i + '" href="#">Clique aqui</a></td>';
            } else {
                content += '<td class="ico hide-mobile"></td>';
            }
            content+='<td>' + [(_data[i].dtRegister) ? Functions.formatDateFull(_data[i].dtRegister) : "---"] + '</td>';
            content+='<td>' + [(_data[i].storeName) ? _data[i].storeName : "---"] + '</td>';
            content+='<td>' + [(_data[i].name) ? _data[i].name : "---"]+ '</td>';
            content+='<td>' + [(_data[i].dtStart) ? Functions.formatDateFull(_data[i].dtStart) : "---"] + '</td>';
            content+='<td>' + [(_data[i].dtEnd) ? Functions.formatDateFull(_data[i].dtEnd) : "---"] + '</td>';
            if (_data[i].rescued) {
                content+='<td><strong>' + "Resgatado" + '<strong></td></tr>';
            } else if (_data[i].expired) {
                content+='<td><strong>' + "Vencido" + '<strong></td></tr>';
            } else {
                if(Functions.dateIsGt(_data[i].dtStart)) {
                    content+='<td><strong>' + "A ativar" + '<strong></td></tr>';
                } else {
                    content+='<td><strong>' + "Disponível" + '<strong></td></tr>';
                }
            }

        });

        if (!content) {
            $(".txt-carregando").html("<p>Nenhum voucher disponível</p>");
        } else {
            $(".txt-carregando").remove();
            $("#data-points").html(content);
        }

        $(".btn-voucher").unbind().click(function(e){
            e.preventDefault();
            SELF.detailVoucher($(this).attr("data-index-voucher"));
        });

    }

    static detailVoucher(_index){
        const modalVoucher = new Modal();
        modalVoucher.bindActions("#modal-voucher");
        modalVoucher.showModal();
        if ($(window).width()>768){
            Draggable.create("#modal-voucher .container-modal", {
                type:"x,y",
                dragClickables: false,
                edgeResistance: 0.65,
                bounds: $("#modal-voucher .overlay"),
                throwProps: true,
                cursor: "auto"
            });
        }

        const DATA = global.dataVouchers[_index];

        //console.log("DATA", DATA);

        if(Functions.dateIsGt(DATA.dtStart)) {
            $("#voucher-start-date, #voucher-start-date-mob").html(Functions.formatDateFull(DATA.dtStart));
            $(".start-date").css("display", "block");
            if (window.width < 768){
                $(".start-date-mobile").css("display", "block");
            }
        }

        $("#store-name").html(DATA.storeName);
        $("#voucher-description, #voucher-description-mob").html(DATA.name);
        $("#voucher-expiration-date, #voucher-expiration-date-mob").html(Functions.formatDateFull(DATA.dtEnd));
        $("#voucher-code, #voucher-code-mob").html(DATA.codigo);

        $(".js-print").click(function(e){
            e.preventDefault();
            window.print();
        })

    }

};
