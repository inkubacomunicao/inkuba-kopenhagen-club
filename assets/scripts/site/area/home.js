import { Power2, Tween, Timeline } from "gsap/gsap-core";
import { gsap } from "gsap";

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

export default class {
    static bindHomeActions() {
        this.animateHomeActions();
        //abre cadastro
        const url = window.location.hash;
        if (url.includes("cadastro")) {
            $("#btn-register").trigger('click');
            if (getUrlVars().utm_source) gtag('event', 'view', {'event_category': 'qrcode' ,'event_label': getUrlVars().utm_medium + "_" + getUrlVars().utm_campaign });
        } else if (url.includes("dados")) {
            $("#btn-login").trigger('click');
            sessionStorage.setItem("user-complete-register", "true");
            if (getUrlVars().utm_source) gtag('event', 'view', {'event_category': 'qrcode' ,'event_label': getUrlVars().utm_medium + "_" + getUrlVars().utm_campaign });
        } else {
            sessionStorage.removeItem("user-complete-register");
        }
    }

    static animateHomeActions(){

        let controller = new ScrollMagic.Controller();

        const sceneBgHow = new ScrollMagic.Scene({
            triggerElement: "#tsm-thow",
        })
            .setTween(gsap.from("#sm-bg-how", { duration: 1.5, y: -100, opacity: 0.5, ease: Power2.easeOut}))
            // .addIndicators({name: "BG Benefícios"})
            .addTo(controller);

        const sceneTitleHow = new ScrollMagic.Scene({
            triggerElement: "#tsm-thow",
        })
            .setTween(gsap.from("#sm-thow", { duration: 1, y: 50, opacity: 0, ease: Power2.easeOut}))
            // .addIndicators({name: "Título Benefícios"})
            .addTo(controller);

        const sceneStepsHow = new ScrollMagic.Scene({
            triggerElement: "#tsm-benefits-how",
        })
            .setTween(gsap.from('.benefits div', { duration: 1, opacity: 0, x: -30, ease: Power2.easeOut, stagger: 0.1 }))
            // .addIndicators({name: "Benefícios"})
            .addTo(controller);

        const sceneBtnHow = new ScrollMagic.Scene({
            triggerElement: "#sm-thow",
        })
            .setTween(gsap.from('#sm-btn-how', { duration: 1, opacity: 0, y: 100, ease: Power2.easeOut }))
            // .addIndicators({name: "Btn Beneficios"})
            .addTo(controller);

        const sceneShineProd1 = new ScrollMagic.Scene({
            triggerElement: "#tsm-tprod",
        })
            .setTween(gsap.from('#shine-prod-top', { duration: 1, opacity: 0, y: -150, ease: Power2.easeOut }))
            // .addIndicators({name: "Btn Beneficios"})
            .addTo(controller);

        const sceneTitleProd = new ScrollMagic.Scene({
            triggerElement: "#tsm-tprod",
        })
            .setTween(gsap.from('#sm-tprod', { duration: 1, opacity: 0, y: 50, ease: Power2.easeOut }))
            // .addIndicators({name: "Titulo Produtos"})
            .addTo(controller);


        const sceneProducts = new ScrollMagic.Scene({
            triggerElement: "#tsm-products",
        })
            .setTween(gsap.from('#products .product', { duration: 1.2, opacity: 0, x: -30, ease: Power2.easeOut, stagger: 0.2 }))
            // .addIndicators({name: "Benefícios"})
            .addTo(controller);


        const sceneShineProd2 = new ScrollMagic.Scene({
            triggerElement: "#sm-tprod",
        })
            .setTween(gsap.from('#shine-prod-botton', { duration: 1, opacity: 0, y: 150, ease: Power2.easeOut }))
            // .addIndicators({name: "Btn Beneficios"})
            .addTo(controller);

        const sceneShineRegister1 = new ScrollMagic.Scene({
            triggerElement: "#tsm-stopreg",
        })
            .setTween(gsap.from('#shine-reg-top', { duration: 1, opacity: 0, y: -150, ease: Power2.easeOut }))
            // .addIndicators({name: "Btn Beneficios"})
            .addTo(controller);

        const sceneTitleReg = new ScrollMagic.Scene({
            triggerElement: "#tsm-treg",
        })
            .setTween(gsap.from('#sm-treg', { duration: 1.5, opacity: 0, x: -100, ease: Power2.easeOut }))
            // .addIndicators({name: "Titulo Produtos"})
            .addTo(controller);

        const sceneShineRegister2 = new ScrollMagic.Scene({
            triggerElement: "#tsm-treg",
        })
            .setTween(gsap.from('#shine-reg-botton', { duration: 1, opacity: 0, y: 150, ease: Power2.easeOut }))
            // .addIndicators({name: "Btn Beneficios"})
            .addTo(controller);
    }
};
