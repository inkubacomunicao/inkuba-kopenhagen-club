import * as $ from 'jquery';
import Origin from "./modules/origin";
import '../../sass/site/general.scss';

global.$ = global.jQuery = $;

$(window).on('load', () => {

    Origin.parseOrigin();

});
