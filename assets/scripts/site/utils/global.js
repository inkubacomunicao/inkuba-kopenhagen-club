export default class {
    static getURLAPI() {
        return "/api/";
    }

    static getURLAPIV2() {
        return "/api/v2/";
    }

    static bgError() {
        return "#b13a35";
        // return "linear-gradient(to bottom, #a90329 0%,#8f0222 44%,#6d0019 100%)";
    }

    static bgSuccess() {
        return "#fff";
        // return "linear-gradient(to bottom, #00b721 0%,#00ad2e 44%,#129b00 100%)";
    }
}
