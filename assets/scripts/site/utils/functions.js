export default class {

    static formatDateFull(_date) {
        if (_date.includes("-")) {
            const arrDate = _date.substring(0,10).split("-");
            return arrDate[2] + "/"  + arrDate[1] + "/" + arrDate[0];
        } else {
            const arrOnlyDate = _date.split(" ");
            const arrDate = arrOnlyDate[0].split("/");
            return arrDate[1] + "/"  + arrDate[0] + "/" + arrDate[2];
        }
    }

    static formatDate(_date) {
        const arrDate = _date.split("/");
        return arrDate[2] + "-"  + arrDate[1] + "-" + arrDate[0];
    }

    static dateIsGt(_dtStart){
        var date = _dtStart;
        var varDate = new Date(date);
        var today = new Date();
        today.setHours(0,0,0,0);
        if(varDate >= today) {
            return true;
        } else {
            return false;
        }
    }

    static getFormData($form){
        let unindexed_array = $form.serializeArray();
        let indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

    static numberFy(_text) {
        return _text.replace(/\D/g, '');
    }

    static getFirstWord(_str) {
        const n = _str.split(" ");
        return n[0];
    }

    static getLastWord(_str) {
        const n = _str.split(" ");
        return n[n.length - 1];
    }

    static parseJwt(_token) {
        if (_token) {
            let base64Url = _token.split('.')[1];
            let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            let jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

            return JSON.parse(jsonPayload);
        }
    };

    static checkHash() {
        switch (window.location.hash) {
            case "#cadastro":
                $("#btn-register").trigger('click');
                break;

            case "#completar-cadastro":
                $("#btn-complete-registration").click();
                break;

            default:
                break;
        }
    }

    static toReal(val) {
        return val.toLocaleString('pt-BR', {minimumFractionDigits:2, maximumFractionDigits:2});
    }
}
