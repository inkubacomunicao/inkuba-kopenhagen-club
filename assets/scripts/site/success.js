import Login from "./modules/login";
import Lgpd from "./modules/lgpd";

$(window).on('load', () => {
    if (!sessionStorage.getItem("kc-user-complete-lgpd")){
        //MODAL PRIMEIRO ACESSO
        Lgpd.bindLgpdActions();
    } else {
        Login.bindInviteCompleteRegistration();
    }
});
